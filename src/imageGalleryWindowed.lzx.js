LzResourceLibrary.lzfocusbracket_rsrc={ptype:"sr",frames:["lps/components/lz/resources/focus/focus_top_lft.png","lps/components/lz/resources/focus/focus_top_rt.png","lps/components/lz/resources/focus/focus_bot_lft.png","lps/components/lz/resources/focus/focus_bot_rt.png"],width:7,height:7,sprite:"lps/components/lz/resources/focus/focus_top_lft.sprite.png",spriteoffset:0};LzResourceLibrary.lzfocusbracket_shdw={ptype:"sr",frames:["lps/components/lz/resources/focus/focus_top_lft_shdw.png","lps/components/lz/resources/focus/focus_top_rt_shdw.png","lps/components/lz/resources/focus/focus_bot_lft_shdw.png","lps/components/lz/resources/focus/focus_bot_rt_shdw.png"],width:9,height:9,sprite:"lps/components/lz/resources/focus/focus_top_lft_shdw.sprite.png",spriteoffset:7};LzResourceLibrary.lzbutton_face_rsc={ptype:"sr",frames:["lps/components/lz/resources/button/simpleface_up.png","lps/components/lz/resources/button/simpleface_mo.png","lps/components/lz/resources/button/simpleface_dn.png","lps/components/lz/resources/button/autoPng/simpleface_dsbl.png"],width:2,height:18,sprite:"lps/components/lz/resources/button/simpleface_up.sprite.png",spriteoffset:16};LzResourceLibrary.lzbutton_bezel_inner_rsc={ptype:"sr",frames:["lps/components/lz/resources/autoPng/bezel_inner_up.png","lps/components/lz/resources/autoPng/bezel_inner_up.png","lps/components/lz/resources/autoPng/bezel_inner_dn.png","lps/components/lz/resources/autoPng/outline_dsbl.png"],width:500,height:500,sprite:"lps/components/lz/resources/autoPng/bezel_inner_up.sprite.png",spriteoffset:34};LzResourceLibrary.lzbutton_bezel_outer_rsc={ptype:"sr",frames:["lps/components/lz/resources/autoPng/bezel_outer_up.png","lps/components/lz/resources/autoPng/bezel_outer_up.png","lps/components/lz/resources/autoPng/bezel_outer_dn.png","lps/components/lz/resources/autoPng/transparent.png","lps/components/lz/resources/autoPng/default_outline.png"],width:500,height:500,sprite:"lps/components/lz/resources/autoPng/bezel_outer_up.sprite.png",spriteoffset:534};LzResourceLibrary.window_TL_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/top_lft_slct.png","lps/components/lz/resources/window/top_lft_dslct.png","lps/components/lz/resources/window/top_lft_slct.png","lps/components/lz/resources/window/top_lft_dsbl.png","lps/components/lz/resources/window/top_lft_slct.png"],width:17,height:14,sprite:"lps/components/lz/resources/window/top_lft_slct.sprite.png",spriteoffset:1034};LzResourceLibrary.window_TM_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/top_mid_slct.png","lps/components/lz/resources/window/top_mid_dslct.png","lps/components/lz/resources/window/top_mid_slct.png","lps/components/lz/resources/window/top_mid_dsbl.png","lps/components/lz/resources/window/top_mid_slct.png"],width:5,height:14,sprite:"lps/components/lz/resources/window/top_mid_slct.sprite.png",spriteoffset:1048};LzResourceLibrary.window_TR_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/top_rt_slct.png","lps/components/lz/resources/window/top_rt_dslct.png","lps/components/lz/resources/window/top_rt_drag.png","lps/components/lz/resources/window/top_rt_dsbl.png","lps/components/lz/resources/window/top_rt_slct.png"],width:15,height:14,sprite:"lps/components/lz/resources/window/top_rt_slct.sprite.png",spriteoffset:1062};LzResourceLibrary.window_ML_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/mid_lft_slct.png","lps/components/lz/resources/window/mid_lft_dslct.png","lps/components/lz/resources/window/mid_lft_slct.png","lps/components/lz/resources/window/mid_lft_dsbl.png","lps/components/lz/resources/window/mid_lft_slct.png"],width:17,height:5,sprite:"lps/components/lz/resources/window/mid_lft_slct.sprite.png",spriteoffset:1076};LzResourceLibrary.window_MM_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/mid_mid_slct.png","lps/components/lz/resources/window/mid_mid_dslct.png","lps/components/lz/resources/window/mid_mid_slct.png","lps/components/lz/resources/window/mid_mid_dsbl.png","lps/components/lz/resources/window/mid_mid_slct.png"],width:10,height:10,sprite:"lps/components/lz/resources/window/mid_mid_slct.sprite.png",spriteoffset:1081};LzResourceLibrary.window_MR_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/mid_rt_slct.png","lps/components/lz/resources/window/mid_rt_dslct.png","lps/components/lz/resources/window/mid_rt_drag.png","lps/components/lz/resources/window/mid_rt_dsbl.png","lps/components/lz/resources/window/mid_rt_slct.png"],width:15,height:5,sprite:"lps/components/lz/resources/window/mid_rt_slct.sprite.png",spriteoffset:1091};LzResourceLibrary.window_BL_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/bot_lft_slct.png","lps/components/lz/resources/window/bot_lft_dslct.png","lps/components/lz/resources/window/bot_lft_drag.png","lps/components/lz/resources/window/bot_lft_dsbl.png","lps/components/lz/resources/window/bot_lft_slct.png"],width:17,height:18,sprite:"lps/components/lz/resources/window/bot_lft_slct.sprite.png",spriteoffset:1096};LzResourceLibrary.window_BM_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/bot_mid_slct.png","lps/components/lz/resources/window/bot_mid_dslct.png","lps/components/lz/resources/window/bot_mid_drag.png","lps/components/lz/resources/window/bot_mid_dsbl.png","lps/components/lz/resources/window/bot_mid_slct.png"],width:5,height:18,sprite:"lps/components/lz/resources/window/bot_mid_slct.sprite.png",spriteoffset:1114};LzResourceLibrary.window_BR_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/bot_rt_slct.png","lps/components/lz/resources/window/bot_rt_dslct.png","lps/components/lz/resources/window/bot_rt_drag.png","lps/components/lz/resources/window/bot_rt_dsbl.png","lps/components/lz/resources/window/bot_rt_slct.png"],width:15,height:19,sprite:"lps/components/lz/resources/window/bot_rt_slct.sprite.png",spriteoffset:1132};LzResourceLibrary.window_gripper_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/gripper_slct.png","lps/components/lz/resources/window/gripper_dslct.png","lps/components/lz/resources/window/gripper_dsbl.png"],width:569,height:11,sprite:"lps/components/lz/resources/window/gripper_slct.sprite.png",spriteoffset:1151};LzResourceLibrary.window_closebtn_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/autoPng/closebtn_up.png","lps/components/lz/resources/window/autoPng/closebtn_mo.png","lps/components/lz/resources/window/autoPng/closebtn_dn.png","lps/components/lz/resources/window/autoPng/closebtn_dsbl.png"],width:14,height:14,sprite:"lps/components/lz/resources/window/autoPng/closebtn_up.sprite.png",spriteoffset:1162};LzResourceLibrary.window_resizebtn_rsc={ptype:"sr",frames:["lps/components/lz/resources/window/autoPng/resizebtn_up.png","lps/components/lz/resources/window/autoPng/resizebtn_mo.png","lps/components/lz/resources/window/autoPng/resizebtn_dn.png","lps/components/lz/resources/window/autoPng/resizebtn_dsbl.png"],width:11,height:11,sprite:"lps/components/lz/resources/window/autoPng/resizebtn_up.sprite.png",spriteoffset:1176};LzResourceLibrary.$LZ1={ptype:"ar",frames:["image/zoom-bkgnd-grey.png"],width:132,height:31,spriteoffset:1187};LzResourceLibrary.$LZ2={ptype:"ar",frames:["image/zoom_fit_best.png"],width:27,height:26,spriteoffset:1218};LzResourceLibrary.$LZ3={ptype:"ar",frames:["image/window_nofullscreen.png"],width:27,height:25,spriteoffset:1244};LzResourceLibrary.$LZ4={ptype:"ar",frames:["image/player_rew.png"],width:28,height:28,spriteoffset:1269};LzResourceLibrary.$LZ5={ptype:"ar",frames:["image/player_fwd.png"],width:28,height:28,spriteoffset:1297};LzResourceLibrary.$LZ6={ptype:"ar",frames:["image/spinner/pict2.png","image/spinner/pict3.png","image/spinner/pict4.png","image/spinner/pict1.png","image/spinner/pict5.png","image/spinner/pict6.png"],width:119,height:119,sprite:"image/spinner/pict2.sprite.png",spriteoffset:1325};LzResourceLibrary.__allcss={path:"imageGalleryWindowed.sprite.png"};var appData=null;var appContainer=null;var OMWApp=null;var photoscontainer=null;var tls=null;var gPhV=null;Class.make("$lzc$class_m2",LzCanvas,["$m1",function($0){
with(this){
var $1=lz.Browser.getInitArg("dataLocation");var $2=lz.Browser.getInitArg("appStyle");var $3=lz.Browser.getInitArg("appWidth");var $4=lz.Browser.getInitArg("appHeight");var $5=lz.Browser.getInitArg("autoPlay");var $6=lz.Browser.getInitArg("allowDrag");var $7=lz.Browser.getInitArg("allowDownload");if($1!=null&&$1!="undefined"){
appData.setAttribute("src",$1);Debug.write("Loading customized dataset:")
}else{
appData.setAttribute("src","http:data/gallery.xml");Debug.write("Loading default dataset:")
};if($2!=null&&$2!="undefined"){
OMWApp.appStyle=styleSwitcher($2)
}else{
OMWApp.appStyle=new (lz.whiteStyle)();Debug.write("Style not selected using default whiteStyle")
};if($3!=null&&$3!="undefined"){
canvas.setAttribute("width",$3)
}else{
Debug.write("App Width not selected using default")
};if($4!=null&&$4!="undefined"){
canvas.setAttribute("height",$4)
}else{
Debug.write("App Height not selected using default")
};if($5=="true"){
OMWApp.setAttribute("startPlaying",true)
}else{
Debug.write("autoPlay not selected using default")
};if($6=="false"){
appContainer.setAttribute("allowdrag",false)
}else{
Debug.write("allowDrag not selected using default")
};this.appData.doRequest();Debug.write(this.appData.src+" Loaded.");OMWApp.appStyle.setAttribute("isdefault",true)
}},"styleSwitcher",function($0){
with(this){
Debug.write("Called library::styleSwitcher()");var $1;switch($0){
case "fuzzy":
$1=new (lz.fuzzyStyle)();break;
case "yellow":
$1=new (lz.yellowStyle)();break;
case "red":
$1=new (lz.redStyle)();break;
case "white":
$1=new (lz.whiteStyle)();break;
case "silver":
$1=new (lz.silverStyle)();break;
case "blue":
$1=new (lz.blueStyle)();break;
case "green":
$1=new (lz.greenStyle)();break;
case "gold":
$1=new (lz.goldStyle)();break;
case "purple":
$1=new (lz.purpleStyle)();break;
default:
$1=new (lz.whiteStyle)();Debug.write("Invalid custom style "+$0+" using default openlaszlo style "+$1);

};Debug.write("Selected custom style "+$0+" == "+$1);return $1
}}],["attributes",new LzInheritedHash(LzCanvas.attributes)]);canvas=new $lzc$class_m2(null,{$delegates:["oninit","$m1",null],__LZproxied:"true",appbuilddate:"2009-12-25T15:10:48Z",bgcolor:16777215,embedfonts:true,fontname:"Verdana,Vera,sans-serif",fontsize:11,fontstyle:"plain",height:800,lpsbuild:"trunk@15373 (15373)",lpsbuilddate:"2009-12-25T09:18:51Z",lpsrelease:"Latest",lpsversion:"4.7.x",proxied:false,runtime:"dhtml",title:"Open Multimedia Web Suite :: Photo Gallery",width:900});lz.colors.offwhite=15921906;lz.colors.gray10=1710618;lz.colors.gray20=3355443;lz.colors.gray30=5066061;lz.colors.gray40=6710886;lz.colors.gray50=8355711;lz.colors.gray60=10066329;lz.colors.gray70=11776947;lz.colors.gray80=13421772;lz.colors.gray90=15066597;lz.colors.iceblue1=3298963;lz.colors.iceblue2=5472718;lz.colors.iceblue3=12240085;lz.colors.iceblue4=14017779;lz.colors.iceblue5=15659509;lz.colors.palegreen1=4290113;lz.colors.palegreen2=11785139;lz.colors.palegreen3=12637341;lz.colors.palegreen4=13888170;lz.colors.palegreen5=15725032;lz.colors.gold1=9331721;lz.colors.gold2=13349195;lz.colors.gold3=15126388;lz.colors.gold4=16311446;lz.colors.sand1=13944481;lz.colors.sand2=14276546;lz.colors.sand3=15920859;lz.colors.sand4=15986401;lz.colors.ltpurple1=6575768;lz.colors.ltpurple2=12038353;lz.colors.ltpurple3=13353453;lz.colors.ltpurple4=15329264;lz.colors.grayblue=12501704;lz.colors.graygreen=12635328;lz.colors.graypurple=10460593;lz.colors.ltblue=14540287;lz.colors.ltgreen=14548957;Class.make("$lzc$class_basefocusview",LzView,["active",void 0,"$lzc$set_active",function($0){
with(this){
setActive($0)
}},"target",void 0,"$lzc$set_target",function($0){
with(this){
setTarget($0)
}},"duration",void 0,"_animatorcounter",void 0,"ontarget",void 0,"_nexttarget",void 0,"onactive",void 0,"_xydelegate",void 0,"_widthdel",void 0,"_heightdel",void 0,"_delayfadeoutDL",void 0,"_dofadeout",void 0,"_onstopdel",void 0,"reset",function(){
with(this){
this.setAttribute("x",0);this.setAttribute("y",0);this.setAttribute("width",canvas.width);this.setAttribute("height",canvas.height);setTarget(null)
}},"setActive",function($0){
this.active=$0;if(this.onactive)this.onactive.sendEvent($0)
},"doFocus",function($0){
with(this){
this._dofadeout=false;this.bringToFront();if(this.target)this.setTarget(null);this.setAttribute("visibility",this.active?"visible":"hidden");this._nexttarget=$0;if(visible){
this._animatorcounter+=1;var $1=null;var $2;var $3;var $4;var $5;if($0["getFocusRect"])$1=$0.getFocusRect();if($1){
$2=$1[0];$3=$1[1];$4=$1[2];$5=$1[3]
}else{
$2=$0.getAttributeRelative("x",canvas);$3=$0.getAttributeRelative("y",canvas);$4=$0.getAttributeRelative("width",canvas);$5=$0.getAttributeRelative("height",canvas)
};var $6=this.animate("x",$2,duration);this.animate("y",$3,duration);this.animate("width",$4,duration);this.animate("height",$5,duration);if(this.capabilities["minimize_opacity_changes"]){
this.setAttribute("visibility","visible")
}else{
this.animate("opacity",1,500)
};if(!this._onstopdel)this._onstopdel=new LzDelegate(this,"stopanim");this._onstopdel.register($6,"onstop")
};if(this._animatorcounter<1){
this.setTarget(this._nexttarget);var $1=null;var $2;var $3;var $4;var $5;if($0["getFocusRect"])$1=$0.getFocusRect();if($1){
$2=$1[0];$3=$1[1];$4=$1[2];$5=$1[3]
}else{
$2=$0.getAttributeRelative("x",canvas);$3=$0.getAttributeRelative("y",canvas);$4=$0.getAttributeRelative("width",canvas);$5=$0.getAttributeRelative("height",canvas)
};this.setAttribute("x",$2);this.setAttribute("y",$3);this.setAttribute("width",$4);this.setAttribute("height",$5)
}}},"stopanim",function($0){
with(this){
this._animatorcounter-=1;if(this._animatorcounter<1){
this._dofadeout=true;if(!this._delayfadeoutDL)this._delayfadeoutDL=new LzDelegate(this,"fadeout");lz.Timer.addTimer(this._delayfadeoutDL,1000);this.setTarget(_nexttarget);this._onstopdel.unregisterAll()
}}},"fadeout",function($0){
with(this){
if(_dofadeout){
if(this.capabilities["minimize_opacity_changes"]){
this.setAttribute("visibility","hidden")
}else{
this.animate("opacity",0,500)
}};this._delayfadeoutDL.unregisterAll()
}},"setTarget",function($0){
with(this){
this.target=$0;if(!this._xydelegate){
this._xydelegate=new LzDelegate(this,"followXY")
}else{
this._xydelegate.unregisterAll()
};if(!this._widthdel){
this._widthdel=new LzDelegate(this,"followWidth")
}else{
this._widthdel.unregisterAll()
};if(!this._heightdel){
this._heightdel=new LzDelegate(this,"followHeight")
}else{
this._heightdel.unregisterAll()
};if(this.target==null)return;var $1=$0;var $2=0;while($1!=canvas){
this._xydelegate.register($1,"onx");this._xydelegate.register($1,"ony");$1=$1.immediateparent;$2++
};this._widthdel.register($0,"onwidth");this._heightdel.register($0,"onheight");followXY(null);followWidth(null);followHeight(null)
}},"followXY",function($0){
with(this){
var $1=null;if(target["getFocusRect"])$1=target.getFocusRect();if($1){
this.setAttribute("x",$1[0]);this.setAttribute("y",$1[1])
}else{
this.setAttribute("x",this.target.getAttributeRelative("x",canvas));this.setAttribute("y",this.target.getAttributeRelative("y",canvas))
}}},"followWidth",function($0){
with(this){
var $1=null;if(target["getFocusRect"])$1=target.getFocusRect();if($1){
this.setAttribute("width",$1[2])
}else{
this.setAttribute("width",this.target.width)
}}},"followHeight",function($0){
with(this){
var $1=null;if(target["getFocusRect"])$1=target.getFocusRect();if($1){
this.setAttribute("height",$1[3])
}else{
this.setAttribute("height",this.target.height)
}}},"$m3",function(){
with(this){
var $0=lz.Focus;return $0
}},"$m4",function($0){
with(this){
this.setActive(lz.Focus.focuswithkey);if($0){
this.doFocus($0)
}else{
this.reset();if(this.active){
this.setActive(false)
}}}}],["tagname","basefocusview","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onstop","stopanim",null,"onfocus","$m4","$m3"],_animatorcounter:0,_delayfadeoutDL:null,_dofadeout:false,_heightdel:null,_nexttarget:null,_onstopdel:null,_widthdel:null,_xydelegate:null,active:false,duration:400,initstage:"late",onactive:LzDeclaredEvent,ontarget:LzDeclaredEvent,options:{ignorelayout:true},target:null,visible:false},$lzc$class_basefocusview.attributes)
}}})($lzc$class_basefocusview);Class.make("$lzc$class_m21",LzView,["$m5",function($0){
with(this){
var $1=-classroot.offset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m6",function(){
with(this){
return [classroot,"offset"]
}},"$m7",function($0){
with(this){
var $1=-classroot.offset;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m8",function(){
with(this){
return [classroot,"offset"]
}},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,opacity:0.25,resource:"lzfocusbracket_shdw",x:1,y:1},"class":LzView},{attrs:{$classrootdepth:2,resource:"lzfocusbracket_rsrc"},"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m22",LzView,["$m9",function($0){
with(this){
var $1=parent.width-width+classroot.offset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m10",function(){
with(this){
return [parent,"width",this,"width",classroot,"offset"]
}},"$m11",function($0){
with(this){
var $1=-classroot.offset;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m12",function(){
with(this){
return [classroot,"offset"]
}},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,frame:2,opacity:0.25,resource:"lzfocusbracket_shdw",x:1,y:1},"class":LzView},{attrs:{$classrootdepth:2,frame:2,resource:"lzfocusbracket_rsrc"},"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m23",LzView,["$m13",function($0){
with(this){
var $1=-classroot.offset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m14",function(){
with(this){
return [classroot,"offset"]
}},"$m15",function($0){
with(this){
var $1=parent.height-height+classroot.offset;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m16",function(){
with(this){
return [parent,"height",this,"height",classroot,"offset"]
}},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,frame:3,opacity:0.25,resource:"lzfocusbracket_shdw",x:1,y:1},"class":LzView},{attrs:{$classrootdepth:2,frame:3,resource:"lzfocusbracket_rsrc"},"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m24",LzView,["$m17",function($0){
with(this){
var $1=parent.width-width+classroot.offset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m18",function(){
with(this){
return [parent,"width",this,"width",classroot,"offset"]
}},"$m19",function($0){
with(this){
var $1=parent.height-height+classroot.offset;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m20",function(){
with(this){
return [parent,"height",this,"height",classroot,"offset"]
}},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,frame:4,opacity:0.25,resource:"lzfocusbracket_shdw",x:1,y:1},"class":LzView},{attrs:{$classrootdepth:2,frame:4,resource:"lzfocusbracket_rsrc"},"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_focusoverlay",$lzc$class_basefocusview,["offset",void 0,"topleft",void 0,"topright",void 0,"bottomleft",void 0,"bottomright",void 0,"doFocus",function($0){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["doFocus"]||this.nextMethod(arguments.callee,"doFocus")).call(this,$0);if(visible)this.bounce()
}},"bounce",function(){
with(this){
this.animate("offset",12,duration/2);this.animate("offset",5,duration)
}}],["tagname","focusoverlay","children",[{attrs:{$classrootdepth:1,name:"topleft",x:new LzAlwaysExpr("$m5","$m6"),y:new LzAlwaysExpr("$m7","$m8")},"class":$lzc$class_m21},{attrs:{$classrootdepth:1,name:"topright",x:new LzAlwaysExpr("$m9","$m10"),y:new LzAlwaysExpr("$m11","$m12")},"class":$lzc$class_m22},{attrs:{$classrootdepth:1,name:"bottomleft",x:new LzAlwaysExpr("$m13","$m14"),y:new LzAlwaysExpr("$m15","$m16")},"class":$lzc$class_m23},{attrs:{$classrootdepth:1,name:"bottomright",x:new LzAlwaysExpr("$m17","$m18"),y:new LzAlwaysExpr("$m19","$m20")},"class":$lzc$class_m24}],"attributes",new LzInheritedHash($lzc$class_basefocusview.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({offset:5},$lzc$class_focusoverlay.attributes)
}}})($lzc$class_focusoverlay);Class.make("$lzc$class__componentmanager",LzNode,["focusclass",void 0,"keyhandlers",void 0,"lastsdown",void 0,"lastedown",void 0,"defaults",void 0,"currentdefault",void 0,"defaultstyle",void 0,"ondefaultstyle",void 0,"init",function(){
with(this){
var $0=this.focusclass;if(typeof canvas.focusclass!="undefined"){
$0=canvas.focusclass
};if($0!=null){
canvas.__focus=new (lz[$0])(canvas);canvas.__focus.reset()
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this)
}},"_lastkeydown",void 0,"upkeydel",void 0,"$m25",function(){
with(this){
var $0=lz.Keys;return $0
}},"dispatchKeyDown",function($0){
with(this){
var $1=false;if($0==32){
this.lastsdown=null;var $2=lz.Focus.getFocus();if($2 instanceof lz.basecomponent){
$2.doSpaceDown();this.lastsdown=$2
};$1=true
}else if($0==13&&this.currentdefault){
this.lastedown=this.currentdefault;this.currentdefault.doEnterDown();$1=true
};if($1){
if(!this.upkeydel)this.upkeydel=new LzDelegate(this,"dispatchKeyTimer");this._lastkeydown=$0;lz.Timer.addTimer(this.upkeydel,50)
}}},"dispatchKeyTimer",function($0){
if(this._lastkeydown==32&&this.lastsdown!=null){
this.lastsdown.doSpaceUp();this.lastsdown=null
}else if(this._lastkeydown==13&&this.currentdefault&&this.currentdefault==this.lastedown){
this.currentdefault.doEnterUp()
}},"findClosestDefault",function($0){
with(this){
if(!this.defaults){
return null
};var $1=null;var $2=null;var $3=this.defaults;$0=$0||canvas;var $4=lz.ModeManager.getModalView();for(var $5=0;$5<$3.length;$5++){
var $6=$3[$5];if($4&&!$6.childOf($4)){
continue
};var $7=this.findCommonParent($6,$0);if($7&&(!$1||$7.nodeLevel>$1.nodeLevel)){
$1=$7;$2=$6
}};return $2
}},"findCommonParent",function($0,$1){
while($0.nodeLevel>$1.nodeLevel){
$0=$0.immediateparent;if(!$0.visible)return null
};while($1.nodeLevel>$0.nodeLevel){
$1=$1.immediateparent;if(!$1.visible)return null
};while($0!=$1){
$0=$0.immediateparent;$1=$1.immediateparent;if(!$0.visible||!$1.visible)return null
};return $0
},"makeDefault",function($0){
with(this){
if(!this.defaults)this.defaults=[];this.defaults.push($0);this.checkDefault(lz.Focus.getFocus())
}},"unmakeDefault",function($0){
with(this){
if(!this.defaults)return;for(var $1=0;$1<this.defaults.length;$1++){
if(this.defaults[$1]==$0){
this.defaults.splice($1,1);this.checkDefault(lz.Focus.getFocus());return
}}}},"$m26",function(){
with(this){
var $0=lz.Focus;return $0
}},"checkDefault",function($0){
with(this){
if(!($0 instanceof lz.basecomponent)||!$0.doesenter){
if($0 instanceof lz.inputtext&&$0.multiline){
$0=null
}else{
$0=this.findClosestDefault($0)
}};if($0==this.currentdefault)return;if(this.currentdefault){
this.currentdefault.setAttribute("hasdefault",false)
};this.currentdefault=$0;if($0){
$0.setAttribute("hasdefault",true)
}}},"$m27",function(){
with(this){
var $0=lz.ModeManager;return $0
}},"$m28",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(lz.Focus.getFocus()==null){
this.checkDefault(null)
}}},"setDefaultStyle",function($0){
this.defaultstyle=$0;if(this.ondefaultstyle)this.ondefaultstyle.sendEvent($0)
},"getDefaultStyle",function(){
with(this){
if(this.defaultstyle==null){
this.defaultstyle=new (lz.style)(canvas,{isdefault:true})
};return this.defaultstyle
}}],["tagname","_componentmanager","attributes",new LzInheritedHash(LzNode.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onkeydown","dispatchKeyDown","$m25","onfocus","checkDefault","$m26","onmode","$m28","$m27"],_lastkeydown:0,currentdefault:null,defaults:null,defaultstyle:null,focusclass:"focusoverlay",keyhandlers:null,lastedown:null,lastsdown:null,ondefaultstyle:LzDeclaredEvent,upkeydel:null},$lzc$class__componentmanager.attributes)
}}})($lzc$class__componentmanager);Class.make("$lzc$class_style",LzNode,["isstyle",void 0,"$m29",function($0){
with(this){
this.setAttribute("canvascolor",LzColorUtils.convertColor("null"))
}},"canvascolor",void 0,"$lzc$set_canvascolor",function($0){
with(this){
setCanvasColor($0)
}},"$m30",function($0){
with(this){
this.setAttribute("textcolor",LzColorUtils.convertColor("gray10"))
}},"textcolor",void 0,"$lzc$set_textcolor",function($0){
with(this){
setStyleAttr($0,"textcolor")
}},"$m31",function($0){
with(this){
this.setAttribute("textfieldcolor",LzColorUtils.convertColor("white"))
}},"textfieldcolor",void 0,"$lzc$set_textfieldcolor",function($0){
with(this){
setStyleAttr($0,"textfieldcolor")
}},"$m32",function($0){
with(this){
this.setAttribute("texthilitecolor",LzColorUtils.convertColor("iceblue1"))
}},"texthilitecolor",void 0,"$lzc$set_texthilitecolor",function($0){
with(this){
setStyleAttr($0,"texthilitecolor")
}},"$m33",function($0){
with(this){
this.setAttribute("textselectedcolor",LzColorUtils.convertColor("black"))
}},"textselectedcolor",void 0,"$lzc$set_textselectedcolor",function($0){
with(this){
setStyleAttr($0,"textselectedcolor")
}},"$m34",function($0){
with(this){
this.setAttribute("textdisabledcolor",LzColorUtils.convertColor("gray60"))
}},"textdisabledcolor",void 0,"$lzc$set_textdisabledcolor",function($0){
with(this){
setStyleAttr($0,"textdisabledcolor")
}},"$m35",function($0){
with(this){
this.setAttribute("basecolor",LzColorUtils.convertColor("offwhite"))
}},"basecolor",void 0,"$lzc$set_basecolor",function($0){
with(this){
setStyleAttr($0,"basecolor")
}},"$m36",function($0){
with(this){
this.setAttribute("bgcolor",LzColorUtils.convertColor("white"))
}},"bgcolor",void 0,"$lzc$set_bgcolor",function($0){
with(this){
setStyleAttr($0,"bgcolor")
}},"$m37",function($0){
with(this){
this.setAttribute("hilitecolor",LzColorUtils.convertColor("iceblue4"))
}},"hilitecolor",void 0,"$lzc$set_hilitecolor",function($0){
with(this){
setStyleAttr($0,"hilitecolor")
}},"$m38",function($0){
with(this){
this.setAttribute("selectedcolor",LzColorUtils.convertColor("iceblue3"))
}},"selectedcolor",void 0,"$lzc$set_selectedcolor",function($0){
with(this){
setStyleAttr($0,"selectedcolor")
}},"$m39",function($0){
with(this){
this.setAttribute("disabledcolor",LzColorUtils.convertColor("gray30"))
}},"disabledcolor",void 0,"$lzc$set_disabledcolor",function($0){
with(this){
setStyleAttr($0,"disabledcolor")
}},"$m40",function($0){
with(this){
this.setAttribute("bordercolor",LzColorUtils.convertColor("gray40"))
}},"bordercolor",void 0,"$lzc$set_bordercolor",function($0){
with(this){
setStyleAttr($0,"bordercolor")
}},"$m41",function($0){
this.setAttribute("bordersize",1)
},"bordersize",void 0,"$lzc$set_bordersize",function($0){
with(this){
setStyleAttr($0,"bordersize")
}},"$m42",function($0){
with(this){
this.setAttribute("menuitembgcolor",LzColorUtils.convertColor("textfieldcolor"))
}},"menuitembgcolor",void 0,"isdefault",void 0,"$lzc$set_isdefault",function($0){
with(this){
_setdefault($0)
}},"onisdefault",void 0,"_setdefault",function($0){
with(this){
this.isdefault=$0;if(isdefault){
lz._componentmanager.service.setDefaultStyle(this);if(this["canvascolor"]!=null){
canvas.setAttribute("bgcolor",this.canvascolor)
}};if(this.onisdefault)this.onisdefault.sendEvent(this)
}},"onstylechanged",void 0,"setStyleAttr",function($0,$1){
this[$1]=$0;if(this["on"+$1])this["on"+$1].sendEvent($1);if(this.onstylechanged)this.onstylechanged.sendEvent(this)
},"setCanvasColor",function($0){
with(this){
if(this.isdefault&&$0!=null){
canvas.setAttribute("bgcolor",$0)
};this.canvascolor=$0;if(this.onstylechanged)this.onstylechanged.sendEvent(this)
}},"extend",function($0){
with(this){
var $1=new (lz.style)();$1.canvascolor=this.canvascolor;$1.textcolor=this.textcolor;$1.textfieldcolor=this.textfieldcolor;$1.texthilitecolor=this.texthilitecolor;$1.textselectedcolor=this.textselectedcolor;$1.textdisabledcolor=this.textdisabledcolor;$1.basecolor=this.basecolor;$1.bgcolor=this.bgcolor;$1.hilitecolor=this.hilitecolor;$1.selectedcolor=this.selectedcolor;$1.disabledcolor=this.disabledcolor;$1.bordercolor=this.bordercolor;$1.bordersize=this.bordersize;$1.menuitembgcolor=this.menuitembgcolor;$1.isdefault=this.isdefault;for(var $2 in $0){
$1[$2]=$0[$2]
};new LzDelegate($1,"_forwardstylechanged",this,"onstylechanged");return $1
}},"_forwardstylechanged",function($0){
if(this.onstylechanged)this.onstylechanged.sendEvent(this)
}],["tagname","style","attributes",new LzInheritedHash(LzNode.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({basecolor:new LzOnceExpr("$m35"),bgcolor:new LzOnceExpr("$m36"),bordercolor:new LzOnceExpr("$m40"),bordersize:new LzOnceExpr("$m41"),canvascolor:new LzOnceExpr("$m29"),disabledcolor:new LzOnceExpr("$m39"),hilitecolor:new LzOnceExpr("$m37"),isdefault:false,isstyle:true,menuitembgcolor:new LzOnceExpr("$m42"),onisdefault:LzDeclaredEvent,onstylechanged:LzDeclaredEvent,selectedcolor:new LzOnceExpr("$m38"),textcolor:new LzOnceExpr("$m30"),textdisabledcolor:new LzOnceExpr("$m34"),textfieldcolor:new LzOnceExpr("$m31"),texthilitecolor:new LzOnceExpr("$m32"),textselectedcolor:new LzOnceExpr("$m33")},$lzc$class_style.attributes)
}}})($lzc$class_style);canvas.LzInstantiateView({"class":lz.script,attrs:{script:function(){
lz._componentmanager.service=new (lz._componentmanager)(canvas,null,null,true)
}}},1);Class.make("$lzc$class_whitestyle",$lzc$class_style,null,["tagname","whitestyle","attributes",new LzInheritedHash($lzc$class_style.attributes)]);Class.make("$lzc$class_silverstyle",$lzc$class_style,["$m43",function($0){
with(this){
this.setAttribute("canvascolor",LzColorUtils.convertColor("silver"))
}},"$m44",function($0){
with(this){
this.setAttribute("basecolor",LzColorUtils.convertColor("silver"))
}},"$m45",function($0){
with(this){
this.setAttribute("bgcolor",LzColorUtils.convertColor("offwhite"))
}},"$m46",function($0){
with(this){
this.setAttribute("textfieldcolor",LzColorUtils.convertColor("offwhite"))
}},"$m47",function($0){
with(this){
this.setAttribute("textcolor",LzColorUtils.convertColor("black"))
}},"$m48",function($0){
with(this){
this.setAttribute("texthilitecolor",LzColorUtils.convertColor("iceblue1"))
}},"$m49",function($0){
with(this){
this.setAttribute("textselectedcolor",LzColorUtils.convertColor("white"))
}},"$m50",function($0){
with(this){
this.setAttribute("textdisabledcolor",LzColorUtils.convertColor("gray50"))
}},"$m51",function($0){
with(this){
this.setAttribute("hilitecolor",LzColorUtils.convertColor("gray80"))
}},"$m52",function($0){
with(this){
this.setAttribute("selectedcolor",LzColorUtils.convertColor("gray70"))
}},"$m53",function($0){
with(this){
this.setAttribute("bordercolor",LzColorUtils.convertColor("gray20"))
}},"$m54",function($0){
this.setAttribute("bordersize",1)
}],["tagname","silverstyle","attributes",new LzInheritedHash($lzc$class_style.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({basecolor:new LzOnceExpr("$m44"),bgcolor:new LzOnceExpr("$m45"),bordercolor:new LzOnceExpr("$m53"),bordersize:new LzOnceExpr("$m54"),canvascolor:new LzOnceExpr("$m43"),hilitecolor:new LzOnceExpr("$m51"),selectedcolor:new LzOnceExpr("$m52"),textcolor:new LzOnceExpr("$m47"),textdisabledcolor:new LzOnceExpr("$m50"),textfieldcolor:new LzOnceExpr("$m46"),texthilitecolor:new LzOnceExpr("$m48"),textselectedcolor:new LzOnceExpr("$m49")},$lzc$class_silverstyle.attributes)
}}})($lzc$class_silverstyle);Class.make("$lzc$class_bluestyle",$lzc$class_style,["$m55",function($0){
with(this){
this.setAttribute("canvascolor",LzColorUtils.convertColor("grayblue"))
}},"$m56",function($0){
with(this){
this.setAttribute("basecolor",LzColorUtils.convertColor("iceblue3"))
}},"$m57",function($0){
with(this){
this.setAttribute("bgcolor",LzColorUtils.convertColor("iceblue5"))
}},"$m58",function($0){
with(this){
this.setAttribute("textfieldcolor",LzColorUtils.convertColor("offwhite"))
}},"$m59",function($0){
with(this){
this.setAttribute("textcolor",LzColorUtils.convertColor("black"))
}},"$m60",function($0){
with(this){
this.setAttribute("texthilitecolor",LzColorUtils.convertColor("iceblue1"))
}},"$m61",function($0){
with(this){
this.setAttribute("textselectedcolor",LzColorUtils.convertColor("white"))
}},"$m62",function($0){
with(this){
this.setAttribute("textdisabledcolor",LzColorUtils.convertColor("gray50"))
}},"$m63",function($0){
with(this){
this.setAttribute("hilitecolor",LzColorUtils.convertColor("iceblue4"))
}},"$m64",function($0){
with(this){
this.setAttribute("selectedcolor",LzColorUtils.convertColor("iceblue2"))
}},"$m65",function($0){
with(this){
this.setAttribute("bordercolor",LzColorUtils.convertColor("gray30"))
}},"$m66",function($0){
this.setAttribute("bordersize",1)
}],["tagname","bluestyle","attributes",new LzInheritedHash($lzc$class_style.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({basecolor:new LzOnceExpr("$m56"),bgcolor:new LzOnceExpr("$m57"),bordercolor:new LzOnceExpr("$m65"),bordersize:new LzOnceExpr("$m66"),canvascolor:new LzOnceExpr("$m55"),hilitecolor:new LzOnceExpr("$m63"),selectedcolor:new LzOnceExpr("$m64"),textcolor:new LzOnceExpr("$m59"),textdisabledcolor:new LzOnceExpr("$m62"),textfieldcolor:new LzOnceExpr("$m58"),texthilitecolor:new LzOnceExpr("$m60"),textselectedcolor:new LzOnceExpr("$m61")},$lzc$class_bluestyle.attributes)
}}})($lzc$class_bluestyle);Class.make("$lzc$class_greenstyle",$lzc$class_style,["$m67",function($0){
with(this){
this.setAttribute("canvascolor",LzColorUtils.convertColor("graygreen"))
}},"$m68",function($0){
with(this){
this.setAttribute("basecolor",LzColorUtils.convertColor("palegreen2"))
}},"$m69",function($0){
with(this){
this.setAttribute("bgcolor",LzColorUtils.convertColor("palegreen5"))
}},"$m70",function($0){
with(this){
this.setAttribute("textfieldcolor",LzColorUtils.convertColor("palegreen5"))
}},"$m71",function($0){
with(this){
this.setAttribute("textcolor",LzColorUtils.convertColor("black"))
}},"$m72",function($0){
with(this){
this.setAttribute("texthilitecolor",LzColorUtils.convertColor("palegreen1"))
}},"$m73",function($0){
with(this){
this.setAttribute("textselectedcolor",LzColorUtils.convertColor("gray10"))
}},"$m74",function($0){
with(this){
this.setAttribute("textdisabledcolor",LzColorUtils.convertColor("gray40"))
}},"$m75",function($0){
with(this){
this.setAttribute("hilitecolor",LzColorUtils.convertColor("palegreen4"))
}},"$m76",function($0){
with(this){
this.setAttribute("selectedcolor",LzColorUtils.convertColor("palegreen3"))
}}],["tagname","greenstyle","attributes",new LzInheritedHash($lzc$class_style.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({basecolor:new LzOnceExpr("$m68"),bgcolor:new LzOnceExpr("$m69"),canvascolor:new LzOnceExpr("$m67"),hilitecolor:new LzOnceExpr("$m75"),selectedcolor:new LzOnceExpr("$m76"),textcolor:new LzOnceExpr("$m71"),textdisabledcolor:new LzOnceExpr("$m74"),textfieldcolor:new LzOnceExpr("$m70"),texthilitecolor:new LzOnceExpr("$m72"),textselectedcolor:new LzOnceExpr("$m73")},$lzc$class_greenstyle.attributes)
}}})($lzc$class_greenstyle);Class.make("$lzc$class_goldstyle",$lzc$class_style,["$m77",function($0){
with(this){
this.setAttribute("canvascolor",LzColorUtils.convertColor("sand2"))
}},"$m78",function($0){
with(this){
this.setAttribute("basecolor",LzColorUtils.convertColor("sand1"))
}},"$m79",function($0){
with(this){
this.setAttribute("bgcolor",LzColorUtils.convertColor("sand4"))
}},"$m80",function($0){
with(this){
this.setAttribute("textfieldcolor",LzColorUtils.convertColor("sand3"))
}},"$m81",function($0){
with(this){
this.setAttribute("textcolor",LzColorUtils.convertColor("gray10"))
}},"$m82",function($0){
with(this){
this.setAttribute("texthilitecolor",LzColorUtils.convertColor("gold1"))
}},"$m83",function($0){
with(this){
this.setAttribute("textselectedcolor",LzColorUtils.convertColor("white"))
}},"$m84",function($0){
with(this){
this.setAttribute("textdisabledcolor",LzColorUtils.convertColor("gray40"))
}},"$m85",function($0){
with(this){
this.setAttribute("hilitecolor",LzColorUtils.convertColor("gold4"))
}},"$m86",function($0){
with(this){
this.setAttribute("selectedcolor",LzColorUtils.convertColor("gold2"))
}}],["tagname","goldstyle","attributes",new LzInheritedHash($lzc$class_style.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({basecolor:new LzOnceExpr("$m78"),bgcolor:new LzOnceExpr("$m79"),canvascolor:new LzOnceExpr("$m77"),hilitecolor:new LzOnceExpr("$m85"),selectedcolor:new LzOnceExpr("$m86"),textcolor:new LzOnceExpr("$m81"),textdisabledcolor:new LzOnceExpr("$m84"),textfieldcolor:new LzOnceExpr("$m80"),texthilitecolor:new LzOnceExpr("$m82"),textselectedcolor:new LzOnceExpr("$m83")},$lzc$class_goldstyle.attributes)
}}})($lzc$class_goldstyle);Class.make("$lzc$class_purplestyle",$lzc$class_style,["$m87",function($0){
with(this){
this.setAttribute("canvascolor",LzColorUtils.convertColor("graypurple"))
}},"$m88",function($0){
with(this){
this.setAttribute("basecolor",LzColorUtils.convertColor("ltpurple2"))
}},"$m89",function($0){
with(this){
this.setAttribute("bgcolor",LzColorUtils.convertColor("iceblue5"))
}},"$m90",function($0){
with(this){
this.setAttribute("textfieldcolor",LzColorUtils.convertColor("ltpurple4"))
}},"$m91",function($0){
with(this){
this.setAttribute("textcolor",LzColorUtils.convertColor("gray10"))
}},"$m92",function($0){
with(this){
this.setAttribute("texthilitecolor",LzColorUtils.convertColor("ltpurple1"))
}},"$m93",function($0){
with(this){
this.setAttribute("textselectedcolor",LzColorUtils.convertColor("white"))
}},"$m94",function($0){
with(this){
this.setAttribute("textdisabledcolor",LzColorUtils.convertColor("gray40"))
}},"$m95",function($0){
with(this){
this.setAttribute("hilitecolor",LzColorUtils.convertColor("ltpurple3"))
}},"$m96",function($0){
with(this){
this.setAttribute("selectedcolor",LzColorUtils.convertColor("ltpurple2"))
}}],["tagname","purplestyle","attributes",new LzInheritedHash($lzc$class_style.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({basecolor:new LzOnceExpr("$m88"),bgcolor:new LzOnceExpr("$m89"),canvascolor:new LzOnceExpr("$m87"),hilitecolor:new LzOnceExpr("$m95"),selectedcolor:new LzOnceExpr("$m96"),textcolor:new LzOnceExpr("$m91"),textdisabledcolor:new LzOnceExpr("$m94"),textfieldcolor:new LzOnceExpr("$m90"),texthilitecolor:new LzOnceExpr("$m92"),textselectedcolor:new LzOnceExpr("$m93")},$lzc$class_purplestyle.attributes)
}}})($lzc$class_purplestyle);Class.make("$lzc$class_statictext",LzText,null,["tagname","statictext","attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_basecomponent",LzView,["enabled",void 0,"$lzc$set_focusable",function($0){
with(this){
_setFocusable($0)
}},"_focusable",void 0,"onfocusable",void 0,"text",void 0,"doesenter",void 0,"$lzc$set_doesenter",function($0){
this._setDoesEnter($0)
},"$m97",function($0){
var $1=this.enabled&&(this._parentcomponent?this._parentcomponent._enabled:true);if($1!==this["_enabled"]||!this.inited){
this.setAttribute("_enabled",$1)
}},"$m98",function(){
return [this,"enabled",this,"_parentcomponent",this._parentcomponent,"_enabled"]
},"_enabled",void 0,"$lzc$set__enabled",function($0){
this._setEnabled($0)
},"_parentcomponent",void 0,"_initcomplete",void 0,"isdefault",void 0,"$lzc$set_isdefault",function($0){
this._setIsDefault($0)
},"onisdefault",void 0,"hasdefault",void 0,"_setEnabled",function($0){
with(this){
this._enabled=$0;var $1=this._enabled&&this._focusable;if($1!=this.focusable){
this.focusable=$1;if(this.onfocusable.ready)this.onfocusable.sendEvent()
};if(_initcomplete)_showEnabled();if(this.on_enabled.ready)this.on_enabled.sendEvent()
}},"_setFocusable",function($0){
this._focusable=$0;if(this.enabled){
this.focusable=this._focusable;if(this.onfocusable.ready)this.onfocusable.sendEvent()
}else{
this.focusable=false
}},"construct",function($0,$1){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["construct"]||this.nextMethod(arguments.callee,"construct")).call(this,$0,$1);var $2=this.immediateparent;while($2!=canvas){
if(lz.basecomponent["$lzsc$isa"]?lz.basecomponent.$lzsc$isa($2):$2 instanceof lz.basecomponent){
this._parentcomponent=$2;break
};$2=$2.immediateparent
}}},"init",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);this._initcomplete=true;this._mousedownDel=new LzDelegate(this,"_doMousedown",this,"onmousedown");if(this.styleable){
_usestyle()
};if(!this["_enabled"])_showEnabled()
}},"_doMousedown",function($0){},"doSpaceDown",function(){
return false
},"doSpaceUp",function(){
return false
},"doEnterDown",function(){
return false
},"doEnterUp",function(){
return false
},"_setIsDefault",function($0){
with(this){
this.isdefault=this["isdefault"]==true;if(this.isdefault==$0)return;if($0){
lz._componentmanager.service.makeDefault(this)
}else{
lz._componentmanager.service.unmakeDefault(this)
};this.isdefault=$0;if(this.onisdefault.ready){
this.onisdefault.sendEvent($0)
}}},"_setDoesEnter",function($0){
with(this){
this.doesenter=$0;if(lz.Focus.getFocus()==this){
lz._componentmanager.service.checkDefault(this)
}}},"updateDefault",function(){
with(this){
lz._componentmanager.service.checkDefault(lz.Focus.getFocus())
}},"$m99",function($0){
this.setAttribute("style",null)
},"style",void 0,"$lzc$set_style",function($0){
with(this){
styleable?setStyle($0):(this.style=null)
}},"styleable",void 0,"_style",void 0,"onstyle",void 0,"_styledel",void 0,"_otherstyledel",void 0,"setStyle",function($0){
with(this){
if(!styleable)return;if($0!=null&&!$0["isstyle"]){
var $1=this._style;if(!$1){
if(this._parentcomponent){
$1=this._parentcomponent.style
}else $1=lz._componentmanager.service.getDefaultStyle()
};$0=$1.extend($0)
};this._style=$0;if($0==null){
if(!this._otherstyledel){
this._otherstyledel=new LzDelegate(this,"_setstyle")
}else{
this._otherstyledel.unregisterAll()
};if(this._parentcomponent&&this._parentcomponent.styleable){
this._otherstyledel.register(this._parentcomponent,"onstyle");$0=this._parentcomponent.style
}else{
this._otherstyledel.register(lz._componentmanager.service,"ondefaultstyle");$0=lz._componentmanager.service.getDefaultStyle()
}}else if(this._otherstyledel){
this._otherstyledel.unregisterAll();this._otherstyledel=null
};_setstyle($0)
}},"_usestyle",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(this._initcomplete&&this["style"]&&this.style.isinited){
this._applystyle(this.style)
}},"_setstyle",function($0){
with(this){
if(!this._styledel){
this._styledel=new LzDelegate(this,"_usestyle")
}else{
_styledel.unregisterAll()
};if($0){
_styledel.register($0,"onstylechanged")
};this.style=$0;_usestyle();if(this.onstyle.ready)this.onstyle.sendEvent(this.style)
}},"_applystyle",function($0){},"setTint",function($0,$1,$2){
switch(arguments.length){
case 2:
$2=0;

};if($0.capabilities.colortransform){
if($1!=""&&$1!=null){
var $3=$1;var $4=$3>>16&255;var $5=$3>>8&255;var $6=$3&255;$4+=51;$5+=51;$6+=51;$4=$4/255*100;$5=$5/255*100;$6=$6/255*100;$0.setColorTransform({ra:$4,ga:$5,ba:$6,rb:$2,gb:$2,bb:$2})
}}},"on_enabled",void 0,"_showEnabled",function(){},"acceptValue",function($0,$1){
switch(arguments.length){
case 1:
$1=null;

};this.setAttribute("text",$0)
},"presentValue",function($0){
switch(arguments.length){
case 0:
$0=null;

};return this.text
},"$lzc$presentValue_dependencies",function($0,$1,$2){
switch(arguments.length){
case 2:
$2=null;

};return [this,"text"]
},"applyData",function($0){
this.acceptValue($0)
},"updateData",function(){
return this.presentValue()
},"destroy",function(){
with(this){
if(this["isdefault"]&&this.isdefault){
lz._componentmanager.service.unmakeDefault(this)
};if(this._otherstyledel){
this._otherstyledel.unregisterAll();this._otherstyledel=null
};if(this._styledel){
this._styledel.unregisterAll();this._styledel=null
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["destroy"]||this.nextMethod(arguments.callee,"destroy")).call(this)
}},"toString",function(){
var $0="";var $1="";var $2="";if(this["id"]!=null)$0="  id="+this.id;if(this["name"]!=null)$1=' named "'+this.name+'"';if(this["text"]&&this.text!="")$2="  text="+this.text;return this.constructor.tagname+$1+$0+$2
}],["tagname","basecomponent","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_enabled:new LzAlwaysExpr("$m97","$m98"),_focusable:true,_initcomplete:false,_otherstyledel:null,_parentcomponent:null,_style:null,_styledel:null,doesenter:false,enabled:true,focusable:true,hasdefault:false,on_enabled:LzDeclaredEvent,onfocusable:LzDeclaredEvent,onisdefault:LzDeclaredEvent,onstyle:LzDeclaredEvent,style:new LzOnceExpr("$m99"),styleable:true,text:""},$lzc$class_basecomponent.attributes)
}}})($lzc$class_basecomponent);Class.make("$lzc$class_basebutton",$lzc$class_basecomponent,["normalResourceNumber",void 0,"overResourceNumber",void 0,"downResourceNumber",void 0,"disabledResourceNumber",void 0,"$m100",function($0){
this.setAttribute("maxframes",this.totalframes)
},"maxframes",void 0,"resourceviewcount",void 0,"$lzc$set_resourceviewcount",function($0){
this.setResourceViewCount($0)
},"respondtomouseout",void 0,"$m101",function($0){
this.setAttribute("reference",this)
},"reference",void 0,"$lzc$set_reference",function($0){
with(this){
setreference($0)
}},"onresourceviewcount",void 0,"_msdown",void 0,"_msin",void 0,"setResourceViewCount",function($0){
this.resourceviewcount=$0;if(this._initcomplete){
if($0>0){
if(this.subviews){
this.maxframes=this.subviews[0].totalframes;if(this.onresourceviewcount){
this.onresourceviewcount.sendEvent()
}}}}},"_callShow",function(){
if(this._msdown&&this._msin&&this.maxframes>=this.downResourceNumber){
this.showDown()
}else if(this._msin&&this.maxframes>=this.overResourceNumber){
this.showOver()
}else this.showUp()
},"$m102",function(){
with(this){
var $0=lz.ModeManager;return $0
}},"$m103",function($0){
if($0&&(this._msdown||this._msin)&&!this.childOf($0)){
this._msdown=false;this._msin=false;this._callShow()
}},"$lzc$set_frame",function($0){
with(this){
if(this.resourceviewcount>0){
for(var $1=0;$1<resourceviewcount;$1++){
this.subviews[$1].setAttribute("frame",$0)
}}else{
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_frame"]||this.nextMethod(arguments.callee,"$lzc$set_frame")).call(this,$0)
}}},"doSpaceDown",function(){
if(this._enabled){
this.showDown()
}},"doSpaceUp",function(){
if(this._enabled){
this.onclick.sendEvent();this.showUp()
}},"doEnterDown",function(){
if(this._enabled){
this.showDown()
}},"doEnterUp",function(){
if(this._enabled){
if(this.onclick){
this.onclick.sendEvent()
};this.showUp()
}},"$m104",function($0){
if(this.isinited){
this.maxframes=this.totalframes;this._callShow()
}},"init",function(){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);this.setResourceViewCount(this.resourceviewcount);this._callShow()
},"$m105",function($0){
this.setAttribute("_msin",true);this._callShow()
},"$m106",function($0){
this.setAttribute("_msin",false);this._callShow()
},"$m107",function($0){
this.setAttribute("_msdown",true);this._callShow()
},"$m108",function($0){
this.setAttribute("_msdown",false);this._callShow()
},"_showEnabled",function(){
with(this){
reference.setAttribute("clickable",this._enabled);showUp()
}},"showDown",function($0){
switch(arguments.length){
case 0:
$0=null;

};this.setAttribute("frame",this.downResourceNumber)
},"showUp",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(!this._enabled&&this.disabledResourceNumber){
this.setAttribute("frame",this.disabledResourceNumber)
}else{
this.setAttribute("frame",this.normalResourceNumber)
}},"showOver",function($0){
switch(arguments.length){
case 0:
$0=null;

};this.setAttribute("frame",this.overResourceNumber)
},"setreference",function($0){
this.reference=$0;if($0!=this)this.setAttribute("clickable",false)
},"_applystyle",function($0){
with(this){
setTint(this,$0.basecolor)
}}],["tagname","basebutton","attributes",new LzInheritedHash($lzc$class_basecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmode","$m103","$m102","ontotalframes","$m104",null,"onmouseover","$m105",null,"onmouseout","$m106",null,"onmousedown","$m107",null,"onmouseup","$m108",null],_msdown:false,_msin:false,clickable:true,disabledResourceNumber:4,downResourceNumber:3,focusable:false,maxframes:new LzOnceExpr("$m100"),normalResourceNumber:1,onclick:LzDeclaredEvent,onresourceviewcount:LzDeclaredEvent,overResourceNumber:2,reference:new LzOnceExpr("$m101"),resourceviewcount:0,respondtomouseout:true,styleable:false},$lzc$class_basebutton.attributes)
}}})($lzc$class_basebutton);Class.make("$lzc$class_swatchview",LzView,["ctransform",void 0,"color",void 0,"construct",function($0,$1){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["construct"]||this.nextMethod(arguments.callee,"construct")).call(this,$0,$1);this.capabilities=new LzInheritedHash(this.capabilities);this.capabilities.colortransform=true;if($1["width"]==null){
$1["width"]=this.immediateparent.width
};if($1["height"]==null){
$1["height"]=this.immediateparent.height
};if($1["fgcolor"]==null&&$1["bgcolor"]==null){
$1["fgcolor"]=16777215
}}},"$lzc$set_fgcolor",function($0){
this.setAttribute("bgcolor",$0)
},"$lzc$set_bgcolor",function($0){
with(this){
this.color=$0;if(this.ctransform!=null){
var $1=$0>>16&255;var $2=$0>>8&255;var $3=$0&255;$1=$1*ctransform["ra"]/100+ctransform["rb"];$1=Math.min($1,255);$2=$2*ctransform["ga"]/100+ctransform["gb"];$2=Math.min($2,255);$3=$3*ctransform["ba"]/100+ctransform["bb"];$3=Math.min($3,255);$0=Math.floor($3+($2<<8)+($1<<16))
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_bgcolor"]||this.nextMethod(arguments.callee,"$lzc$set_bgcolor")).call(this,$0)
}},"setColorTransform",function($0){
this.ctransform=$0;this.setAttribute("bgcolor",this.color)
}],["tagname","swatchview","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({color:16777215,ctransform:null},$lzc$class_swatchview.attributes)
}}})($lzc$class_swatchview);Class.make("$lzc$class_m154",LzView,["$m120",function($0){
with(this){
var $1=parent.width-1;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m121",function(){
with(this){
return [parent,"width"]
}},"$m122",function($0){
with(this){
var $1=parent.height-1;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m123",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m155",LzView,["$m124",function($0){
with(this){
var $1=parent.width-3;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m125",function(){
with(this){
return [parent,"width"]
}},"$m126",function($0){
with(this){
var $1=parent.height-3;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m127",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m156",LzView,["$m128",function($0){
with(this){
var $1=parent.width-4;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m129",function(){
with(this){
return [parent,"width"]
}},"$m130",function($0){
with(this){
var $1=parent.height-4;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m131",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m157",LzView,["$m132",function($0){
with(this){
var $1=parent.parent.width-2;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m133",function(){
with(this){
return [parent.parent,"width"]
}},"$m134",function($0){
with(this){
var $1=parent.parent.height-2;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m135",function(){
with(this){
return [parent.parent,"height"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m158",LzView,["$m136",function($0){
with(this){
var $1=parent.parent.height-2;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m137",function(){
with(this){
return [parent.parent,"height"]
}},"$m138",function($0){
with(this){
var $1=parent.parent.width-3;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m139",function(){
with(this){
return [parent.parent,"width"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m159",LzView,["$m140",function($0){
with(this){
var $1=parent.parent.width-1;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m141",function(){
with(this){
return [parent.parent,"width"]
}},"$m142",function($0){
with(this){
var $1=parent.parent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m143",function(){
with(this){
return [parent.parent,"height"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m160",LzView,["$m144",function($0){
with(this){
var $1=parent.parent.height-1;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m145",function(){
with(this){
return [parent.parent,"height"]
}},"$m146",function($0){
with(this){
var $1=parent.parent.width-1;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m147",function(){
with(this){
return [parent.parent,"width"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m161",LzText,["$m148",function($0){
with(this){
var $1=parent.text_x+parent.titleshift;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m149",function(){
with(this){
return [parent,"text_x",parent,"titleshift"]
}},"$m150",function($0){
with(this){
var $1=parent.text_y+parent.titleshift;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m151",function(){
with(this){
return [parent,"text_y",parent,"titleshift"]
}},"$m152",function($0){
with(this){
var $1=parent.text;if($1!==this["text"]||!this.inited){
this.setAttribute("text",$1)
}}},"$m153",function(){
with(this){
return [parent,"text"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_button",$lzc$class_basebutton,["text_padding_x",void 0,"text_padding_y",void 0,"$m109",function($0){
var $1=this.width/2-this._title.width/2;if($1!==this["text_x"]||!this.inited){
this.setAttribute("text_x",$1)
}},"$m110",function(){
return [this,"width",this._title,"width"]
},"text_x",void 0,"$m111",function($0){
var $1=this.height/2-this._title.height/2;if($1!==this["text_y"]||!this.inited){
this.setAttribute("text_y",$1)
}},"$m112",function(){
return [this,"height",this._title,"height"]
},"text_y",void 0,"$m113",function($0){
var $1=this._title.width+2*this.text_padding_x;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}},"$m114",function(){
return [this._title,"width",this,"text_padding_x"]
},"$m115",function($0){
var $1=this._title.height+2*this.text_padding_y;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}},"$m116",function(){
return [this._title,"height",this,"text_padding_y"]
},"buttonstate",void 0,"$m117",function($0){
var $1=this.buttonstate==1?0:1;if($1!==this["titleshift"]||!this.inited){
this.setAttribute("titleshift",$1)
}},"$m118",function(){
return [this,"buttonstate"]
},"titleshift",void 0,"leftalign",void 0,"_showEnabled",function(){
with(this){
showUp();setAttribute("clickable",_enabled)
}},"showDown",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(this.hasdefault){
this._outerbezel.setAttribute("frame",5)
}else{
this._outerbezel.setAttribute("frame",this.downResourceNumber)
};this._face.setAttribute("frame",this.downResourceNumber);this._innerbezel.setAttribute("frame",this.downResourceNumber);setAttribute("buttonstate",2)
}},"showUp",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(_enabled){
if(this.hasdefault){
this._outerbezel.setAttribute("frame",5)
}else{
this._outerbezel.setAttribute("frame",this.normalResourceNumber)
};this._face.setAttribute("frame",this.normalResourceNumber);this._innerbezel.setAttribute("frame",this.normalResourceNumber);if(this.style)this._title.setAttribute("fgcolor",this.style.textcolor)
}else{
if(this.style)this._title.setAttribute("fgcolor",this.style.textdisabledcolor);this._face.setAttribute("frame",this.disabledResourceNumber);this._outerbezel.setAttribute("frame",this.disabledResourceNumber);this._innerbezel.setAttribute("frame",this.disabledResourceNumber)
};setAttribute("buttonstate",1)
}},"showOver",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(this.hasdefault){
this._outerbezel.setAttribute("frame",5)
}else{
this._outerbezel.setAttribute("frame",this.overResourceNumber)
};this._face.setAttribute("frame",this.overResourceNumber);this._innerbezel.setAttribute("frame",this.overResourceNumber);setAttribute("buttonstate",1)
}},"$m119",function($0){
with(this){
if(this._initcomplete){
if(this.buttonstate==1)showUp()
}}},"_applystyle",function($0){
with(this){
if(this.style!=null){
this.textcolor=$0.textcolor;this.textdisabledcolor=$0.textdisabledcolor;if(enabled){
_title.setAttribute("fgcolor",$0.textcolor)
}else{
_title.setAttribute("fgcolor",$0.textdisabledcolor)
};setTint(_outerbezel,$0.basecolor);setTint(_innerbezel,$0.basecolor);setTint(_face,$0.basecolor)
}}},"_outerbezel",void 0,"_innerbezel",void 0,"_face",void 0,"_innerbezelbottom",void 0,"_outerbezelbottom",void 0,"_title",void 0],["tagname","button","children",[{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0x919191"),height:new LzAlwaysExpr("$m122","$m123"),name:"_outerbezel",width:new LzAlwaysExpr("$m120","$m121"),x:0,y:0},"class":$lzc$class_m154},{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0xffffff"),height:new LzAlwaysExpr("$m126","$m127"),name:"_innerbezel",width:new LzAlwaysExpr("$m124","$m125"),x:1,y:1},"class":$lzc$class_m155},{attrs:{$classrootdepth:1,height:new LzAlwaysExpr("$m130","$m131"),name:"_face",resource:"lzbutton_face_rsc",stretches:"both",width:new LzAlwaysExpr("$m128","$m129"),x:2,y:2},"class":$lzc$class_m156},{attrs:{$classrootdepth:1,name:"_innerbezelbottom"},children:[{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0x585858"),height:new LzAlwaysExpr("$m134","$m135"),width:1,x:new LzAlwaysExpr("$m132","$m133"),y:1},"class":$lzc$class_m157},{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0x585858"),height:1,width:new LzAlwaysExpr("$m138","$m139"),x:1,y:new LzAlwaysExpr("$m136","$m137")},"class":$lzc$class_m158}],"class":LzView},{attrs:{$classrootdepth:1,name:"_outerbezelbottom"},children:[{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0xffffff"),height:new LzAlwaysExpr("$m142","$m143"),opacity:0.7,width:1,x:new LzAlwaysExpr("$m140","$m141"),y:0},"class":$lzc$class_m159},{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0xffffff"),height:1,opacity:0.7,width:new LzAlwaysExpr("$m146","$m147"),x:0,y:new LzAlwaysExpr("$m144","$m145")},"class":$lzc$class_m160}],"class":LzView},{attrs:{$classrootdepth:1,name:"_title",resize:true,text:new LzAlwaysExpr("$m152","$m153"),x:new LzAlwaysExpr("$m148","$m149"),y:new LzAlwaysExpr("$m150","$m151")},"class":$lzc$class_m161}],"attributes",new LzInheritedHash($lzc$class_basebutton.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onhasdefault","$m119",null],buttonstate:1,clickable:true,doesenter:true,focusable:true,height:new LzAlwaysExpr("$m115","$m116"),leftalign:false,maxframes:4,pixellock:true,styleable:true,text_padding_x:11,text_padding_y:4,text_x:new LzAlwaysExpr("$m109","$m110"),text_y:new LzAlwaysExpr("$m111","$m112"),titleshift:new LzAlwaysExpr("$m117","$m118"),width:new LzAlwaysExpr("$m113","$m114")},$lzc$class_button.attributes)
}}})($lzc$class_button);Class.make("$lzc$class_dragstate",LzState,["drag_axis",void 0,"drag_min_x",void 0,"drag_max_x",void 0,"drag_min_y",void 0,"drag_max_y",void 0,"$m162",void 0,"__dragstate_ydoffset",void 0,"$m163",void 0,"$m164",void 0,"y",void 0,"$m165",void 0,"__dragstate_xdoffset",void 0,"$m166",void 0,"$m167",void 0,"x",void 0,"__dragstate_getnewpos",void 0],["tagname","dragstate","attributes",new LzInheritedHash(LzState.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$m162:function($0){
this.setAttribute("__dragstate_ydoffset",this.yoffset-this.getMouse("y"))
},$m163:function($0){
var $1=this.drag_axis=="both"||this.drag_axis=="y"?this.__dragstate_getnewpos("y",this.immediateparent.getMouse("y")+this.__dragstate_ydoffset):this.y;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}},$m164:function(){
return [this,"drag_axis",this,"__dragstate_ydoffset",this,"y"].concat(this["$lzc$__dragstate_getnewpos_dependencies"]?this["$lzc$__dragstate_getnewpos_dependencies"](this,this,"y",this.immediateparent.getMouse("y")+this.__dragstate_ydoffset):[]).concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"y"):[])
},$m165:function($0){
this.setAttribute("__dragstate_xdoffset",this.xoffset-this.getMouse("x"))
},$m166:function($0){
var $1=this.drag_axis=="both"||this.drag_axis=="x"?this.__dragstate_getnewpos("x",this.immediateparent.getMouse("x")+this.__dragstate_xdoffset):this.x;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}},$m167:function(){
return [this,"drag_axis",this,"__dragstate_xdoffset",this,"x"].concat(this["$lzc$__dragstate_getnewpos_dependencies"]?this["$lzc$__dragstate_getnewpos_dependencies"](this,this,"x",this.immediateparent.getMouse("x")+this.__dragstate_xdoffset):[]).concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"x"):[])
},__dragstate_getnewpos:function($0,$1){
var $2=this["drag_min_"+$0];var $3=this["drag_max_"+$0];if($2!=null&&$1<$2)$1=$2;if($3!=null&&$1>$3)$1=$3;return $1
},__dragstate_xdoffset:new LzOnceExpr("$m165"),__dragstate_ydoffset:new LzOnceExpr("$m162"),drag_axis:"both",drag_max_x:null,drag_max_y:null,drag_min_x:null,drag_min_y:null,x:new LzAlwaysExpr("$m166","$m167"),y:new LzAlwaysExpr("$m163","$m164")},$lzc$class_dragstate.attributes)
}}})($lzc$class_dragstate);Class.make("$lzc$class_resizestate",LzState,["$m168",void 0,"__resize_xroffset",void 0,"$m169",void 0,"$m170",void 0,"width",void 0,"$m171",void 0,"__resize_yroffset",void 0,"$m172",void 0,"$m173",void 0,"height",void 0],["tagname","resizestate","attributes",new LzInheritedHash(LzState.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$m168:function($0){
this.setAttribute("__resize_xroffset",this.x-this.width+this.getMouse("x"))
},$m169:function($0){
var $1=this.immediateparent.getMouse("x")-this.__resize_xroffset;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}},$m170:function(){
return [this,"__resize_xroffset"].concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"x"):[])
},$m171:function($0){
this.setAttribute("__resize_yroffset",this.y-this.height+this.getMouse("y"))
},$m172:function($0){
var $1=this.immediateparent.getMouse("y")-this.__resize_yroffset;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}},$m173:function(){
return [this,"__resize_yroffset"].concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"y"):[])
},__resize_xroffset:new LzOnceExpr("$m168"),__resize_yroffset:new LzOnceExpr("$m171"),height:new LzAlwaysExpr("$m172","$m173"),width:new LzAlwaysExpr("$m169","$m170")},$lzc$class_resizestate.attributes)
}}})($lzc$class_resizestate);Class.make("$lzc$class_resizestatemin",$lzc$class_resizestate,["resize_min_width",void 0,"resize_min_height",void 0,"$m174",void 0,"$m175",void 0,"$m176",void 0,"$m177",void 0],["tagname","resizestatemin","attributes",new LzInheritedHash($lzc$class_resizestate.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$m174:function($0){
with(this){
var $1=Math.max(this.immediateparent.getMouse("x")-__resize_xroffset,resize_min_width||0);if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},$m175:function(){
with(this){
return [this,"__resize_xroffset",this,"resize_min_width"].concat(Math["$lzc$max_dependencies"]?Math["$lzc$max_dependencies"](this,Math,this.immediateparent.getMouse("x")-__resize_xroffset,resize_min_width||0):[]).concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"x"):[])
}},$m176:function($0){
with(this){
var $1=Math.max(this.immediateparent.getMouse("y")-__resize_yroffset,resize_min_height||0);if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},$m177:function(){
with(this){
return [this,"__resize_yroffset",this,"resize_min_height"].concat(Math["$lzc$max_dependencies"]?Math["$lzc$max_dependencies"](this,Math,this.immediateparent.getMouse("y")-__resize_yroffset,resize_min_height||0):[]).concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"y"):[])
}},height:new LzAlwaysExpr("$m176","$m177"),resize_min_height:0,resize_min_width:0,width:new LzAlwaysExpr("$m174","$m175")},$lzc$class_resizestatemin.attributes)
}}})($lzc$class_resizestatemin);Class.make("$lzc$class_basewindow",$lzc$class_basecomponent,["minwidth",void 0,"minheight",void 0,"haswindowfocus",void 0,"$lzc$set_haswindowfocus",function($0){
with(this){
setWindowFocus($0)
}},"onhaswindowfocus",void 0,"state",void 0,"defaultbuttongroup",void 0,"allowdrag",void 0,"_windowDrag",void 0,"setDragPos",function($0,$1){
with(this){
var $2=$1-this[$0+"doffset"];var $3=this[$0]-this["start"+$0];if(Math.abs($3)>3){
setAttribute("state",3)
};return $2
}},"_windowResize",void 0,"construct",function($0,$1){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["construct"]||this.nextMethod(arguments.callee,"construct")).call(this,$0,$1);var $2=this.parent.options["windowlist"];if($2==null||typeof $2=="undefined"){
$2=[];this.parent.setOption("windowlist",$2)
}},"init",function(){
with(this){
if(this.datapath!=null){
this.datapath.setAttribute("datacontrolsvisibility",this.visibility=="collapse")
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);this.mousedown_del=new LzDelegate(this,"_mousedown",this,"onmousedown");this.mouseup_del=new LzDelegate(this,"_mouseup",this,"onmouseup");if(this.visible){
var $0=parent.options["windowlist"];$0.push(this);this.setAttribute("haswindowfocus",true)
}}},"destroy",function(){
if(this.mousedel){
this.mousedel.unregisterAll()
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["destroy"]||this.nextMethod(arguments.callee,"destroy")).call(this)
},"sendInFrontOf",function($0){
with(this){
var $1=parent.options["windowlist"];if(this.visible){
for(var $2=0;$2<$1.length;++$2){
if($1[$2]==$0){
this._removeFromWindowlist();$1.splice($2+1,0,this);break
}}};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["sendInFrontOf"]||this.nextMethod(arguments.callee,"sendInFrontOf")).call(this,$0);if($1.length>0){
$1[$1.length-1].setAttribute("haswindowfocus",true)
}}},"sendBehind",function($0){
with(this){
var $1=parent.options["windowlist"];if(this.visible){
for(var $2=0;$2<$1.length;++$2){
if($1[$2]==$0){
this._removeFromWindowlist();$1.splice($2,0,this);break
}}};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["sendBehind"]||this.nextMethod(arguments.callee,"sendBehind")).call(this,$0);if($1.length>0){
$1[$1.length-1].setAttribute("haswindowfocus",true)
}}},"bringToFront",function(){
with(this){
var $0=parent.options["windowlist"];if(this.visible&&$0.length>0){
if($0[$0.length-1]!=this){
this._removeFromWindowlist();$0.push(this)
}};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["bringToFront"]||this.nextMethod(arguments.callee,"bringToFront")).call(this);if($0.length>0){
$0[$0.length-1].setAttribute("haswindowfocus",true)
}}},"sendToBack",function(){
with(this){
var $0=parent.options["windowlist"];if(this.visible){
if($0.length>0&&$0[0]!=this){
this._removeFromWindowlist();$0.splice(0,0,this)
}};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["sendToBack"]||this.nextMethod(arguments.callee,"sendToBack")).call(this);if($0.length>0){
$0[$0.length-1].setAttribute("haswindowfocus",true)
}}},"_mousedown",function($0){
this.setAttribute("haswindowfocus",true);this._startDrag()
},"_mouseup",function($0){
this._stopDrag()
},"_startDrag",function(){
if(this.allowdrag){
this._windowDrag.setAttribute("applied",true)
}},"_stopDrag",function(){
with(this){
this._windowDrag.setAttribute("applied",false);setAttribute("state",1)
}},"_startResize",function(){
with(this){
this._windowResize.setAttribute("applied",true);setAttribute("state",5)
}},"_stopResize",function(){
with(this){
this._windowResize.setAttribute("applied",false);setAttribute("state",1)
}},"_removeFromWindowlist",function(){
with(this){
var $0=parent.options["windowlist"];for(var $1=0;$1<$0.length;$1++){
if($0[$1]==this){
$0.splice($1,1);break
}}}},"$lzc$set_visible",function($0){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_visible"]||this.nextMethod(arguments.callee,"$lzc$set_visible")).call(this,$0);if($0){
if(this.isinited){
var $1=false;var $2=parent.options["windowlist"];var $3=$2.length;for(var $4=0;$4<$3;++$4){
if($2[$4]==this){
$1=true;break
}};if(!$1){
$2[$3]=this
}};this.setAttribute("haswindowfocus",true)
}else{
this._removeFromWindowlist();if(this["haswindowfocus"]){
this.setAttribute("haswindowfocus",false)
}}}},"mousedel",void 0,"setWindowFocus",function($0){
with(this){
if(this["haswindowfocus"]==$0)return;this.haswindowfocus=$0;if($0){
this.bringToFront();setAttribute("state",1);var $1=parent.options["windowlist"];var $2=$1.length;for(var $3=0;$3<$2;$3++){
var $4=$1[$3];if($4!=this){
if($4.haswindowfocus)$4.setAttribute("haswindowfocus",false)
}};if(!this.mousedel){
this.mousedel=new LzDelegate(this,"_checkmouse")
};this.mousedel.register(lz.GlobalMouse,"onmousedown")
}else{
if(this.mousedel)this.mousedel.unregisterAll();setAttribute("state",2);var $5=false;var $1=parent.options["windowlist"];var $2=$1.length;for(var $3=0;$3<$2;$3++){
var $4=$1[$3];if($4["haswindowfocus"]==true){
$5=true;break
}};if(!$5&&$1.length>0){
var $1=parent.options["windowlist"];var $6=$1[$1.length-1];if($6!=null)$6.setAttribute("haswindowfocus",true)
}};if(this.onhaswindowfocus)this.onhaswindowfocus.sendEvent($0)
}},"_checkmouse",function($0){
with(this){
if($0==null||$0==this)return;if(lz.ModeManager.hasMode(this))return;var $1=$0.searchParents("haswindowfocus");if($1!=null){
$1.setWindowFocus(true)
}}},"close",function(){
with(this){
var $0=Array.prototype.slice.call(arguments,0);this.setAttribute("visible",false)
}},"open",function(){
this.setAttribute("visible",true)
},"$lzc$set_height",function($0){
with(this){
var $1=Math.round($0);(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_height"]||this.nextMethod(arguments.callee,"$lzc$set_height")).call(this,$1)
}},"$lzc$set_width",function($0){
with(this){
var $1=Math.round($0);(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_width"]||this.nextMethod(arguments.callee,"$lzc$set_width")).call(this,$1)
}}],["tagname","basewindow","children",[{attrs:{$classrootdepth:1,$m178:function($0){
this.setAttribute("starty",this.y)
},$m179:function($0){
this.setAttribute("startx",this.x)
},$m180:function($0){
this.setAttribute("ydoffset",this.getMouse("y"))
},$m181:function($0){
this.setAttribute("xdoffset",this.getMouse("x"))
},$m182:function($0){
with(this){
var $1=this.state!=3?setDragPos("y",this.immediateparent.getMouse("y")):this.immediateparent.getMouse("y")-this.ydoffset;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},$m183:function(){
return [this,"state",this,"ydoffset"].concat(this["$lzc$setDragPos_dependencies"]?this["$lzc$setDragPos_dependencies"](this,this,"y",this.immediateparent.getMouse("y")):[]).concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"y"):[])
},$m184:function($0){
with(this){
var $1=this.state!=3?setDragPos("x",this.immediateparent.getMouse("x")):this.immediateparent.getMouse("x")-this.xdoffset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},$m185:function(){
return [this,"state",this,"xdoffset"].concat(this["$lzc$setDragPos_dependencies"]?this["$lzc$setDragPos_dependencies"](this,this,"x",this.immediateparent.getMouse("x")):[]).concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"x"):[])
},name:"_windowDrag",startx:new LzOnceExpr("$m179"),starty:new LzOnceExpr("$m178"),x:new LzAlwaysExpr("$m184","$m185"),xdoffset:new LzOnceExpr("$m181"),y:new LzAlwaysExpr("$m182","$m183"),ydoffset:new LzOnceExpr("$m180")},"class":LzState},{attrs:{$classrootdepth:1,$delegates:["onapply","$m190",null],$m186:function($0){
var $1=this.minwidth;if($1!==this["resize_min_width"]||!this.inited){
this.setAttribute("resize_min_width",$1)
}},$m187:function(){
return [this,"minwidth"]
},$m188:function($0){
var $1=this.minheight;if($1!==this["resize_min_height"]||!this.inited){
this.setAttribute("resize_min_height",$1)
}},$m189:function(){
return [this,"minheight"]
},$m190:function($0){
with(this){
parent.setAttribute("haswindowfocus",true)
}},name:"_windowResize",resize_min_height:new LzAlwaysExpr("$m188","$m189"),resize_min_width:new LzAlwaysExpr("$m186","$m187")},"class":$lzc$class_resizestatemin}],"attributes",new LzInheritedHash($lzc$class_basecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({allowdrag:true,clickable:true,defaultbuttongroup:true,focusable:false,haswindowfocus:false,minheight:50,minwidth:60,mousedel:null,onhaswindowfocus:LzDeclaredEvent,options:{ignorelayout:true},pixellock:true,state:1},$lzc$class_basewindow.attributes)
}}})($lzc$class_basewindow);Class.make("$lzc$class_stableborderlayout",LzLayout,["axis",void 0,"$lzc$set_axis",function($0){
this.setAxis($0)
},"reset",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked||this.subviews&&this.subviews.length<2){
return
};this.subviews[1].setAttribute(this.axis,this.subviews[0][this.sizeAxis]);this.update()
},"update",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked||this.subviews.length<3){
return
};if($0==null){
var $1=this.immediateparent;if($1.usegetbounds){
$1=$1.getBounds()
};$0=$1[this.sizeAxis]
};this.lock();var $2=this.subviews[1];var $3=this.subviews[2];var $4=$3.usegetbounds?$3.getBounds():$3;var $5=$2.usegetbounds?$2.getBounds():$2;$3.setAttribute(this.axis,$0-$4[this.sizeAxis]-0.1);$2.setAttribute(this.sizeAxis,$0-$4[this.sizeAxis]-$5[this.axis]+0.1);this.locked=false
},"addSubview",function($0){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["addSubview"]||this.nextMethod(arguments.callee,"addSubview")).call(this,$0);if(this.subviews.length==2){
var $1=this.subviews[0];if($1.usegetbounds){
$1=$1.getBounds()
};this.subviews[1].setAttribute(this.axis,$1[this.sizeAxis]);$0.setAttribute(this.sizeAxis,0)
}else if(this.subviews.length>2){
this.update()
}},"setAxis",function($0){
this.axis=$0;this.sizeAxis=$0=="x"?"width":"height";if(this.updateDelegate)this.updateDelegate.register(this.immediateparent,"on"+this.sizeAxis)
}],["tagname","stableborderlayout","attributes",new LzInheritedHash(LzLayout.attributes)]);Class.make("$lzc$class_resizeview_x",LzView,["left",void 0,"middle",void 0,"right",void 0,"init",function(){
this.leftview.setAttribute("resource",this.left);this.middleview.setAttribute("resource",this.middle);this.rightview.setAttribute("resource",this.right);(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);if(this.hassetheight){
var $0=[this,"height"];this.applyConstraintMethod("adjheight",$0)
}},"adjheight",function($0){
this.leftview.setAttribute("height",$0);this.middleview.setAttribute("height",$0);this.rightview.setAttribute("height",$0)
},"$lzc$set_frame",function($0){
this.frame=$0;this.onframe.sendEvent($0);this.leftview.setAttribute("frame",$0);this.middleview.setAttribute("frame",$0);this.rightview.setAttribute("frame",$0)
},"leftview",void 0,"middleview",void 0,"rightview",void 0,"sbl",void 0],["tagname","resizeview_x","children",[{attrs:{$classrootdepth:1,name:"leftview",stretches:"height"},"class":LzView},{attrs:{$classrootdepth:1,name:"middleview",stretches:"both"},"class":LzView},{attrs:{$classrootdepth:1,name:"rightview",stretches:"height"},"class":LzView},{attrs:{$classrootdepth:1,axis:"x",name:"sbl"},"class":$lzc$class_stableborderlayout}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_resizeview_y",LzView,["top",void 0,"middle",void 0,"bottom",void 0,"init",function(){
this.topview.setAttribute("resource",this.top);this.middleview.setAttribute("resource",this.middle);this.bottomview.setAttribute("resource",this.bottom);(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);if(this.hassetwidth){
var $0=[this,"width"];this.applyConstraintMethod("adjwidth",$0)
}},"$lzc$set_frame",function($0){
this.frame=$0;this.onframe.sendEvent($0);this.topview.setAttribute("frame",$0);this.middleview.setAttribute("frame",$0);this.bottomview.setAttribute("frame",$0)
},"adjwidth",function($0){
this.topview.setAttribute("width",$0);this.middleview.setAttribute("width",$0);this.bottomview.setAttribute("width",$0)
},"topview",void 0,"middleview",void 0,"bottomview",void 0,"sbl",void 0],["tagname","resizeview_y","children",[{attrs:{$classrootdepth:1,name:"topview",stretches:"width"},"class":LzView},{attrs:{$classrootdepth:1,name:"middleview",stretches:"both"},"class":LzView},{attrs:{$classrootdepth:1,name:"bottomview",stretches:"width"},"class":LzView},{attrs:{$classrootdepth:1,axis:"y",name:"sbl"},"class":$lzc$class_stableborderlayout}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m246",$lzc$class_resizeview_x,["$m199",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m200",function(){
with(this){
return [immediateparent,"width"]
}},"$m201",function($0){
with(this){
var $1=parent.state;if($1!==this["frame"]||!this.inited){
this.setAttribute("frame",$1)
}}},"$m202",function(){
with(this){
return [parent,"state"]
}},"$classrootdepth",void 0],["children",LzNode.mergeChildren([],$lzc$class_resizeview_x["children"]),"attributes",new LzInheritedHash($lzc$class_resizeview_x.attributes)]);Class.make("$lzc$class_m247",$lzc$class_resizeview_x,["$m203",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m204",function(){
with(this){
return [immediateparent,"width"]
}},"$m205",function($0){
with(this){
var $1=parent.state;if($1!==this["frame"]||!this.inited){
this.setAttribute("frame",$1)
}}},"$m206",function(){
with(this){
return [parent,"state"]
}},"$classrootdepth",void 0],["children",LzNode.mergeChildren([],$lzc$class_resizeview_x["children"]),"attributes",new LzInheritedHash($lzc$class_resizeview_x.attributes)]);Class.make("$lzc$class_m248",$lzc$class_resizeview_x,["$m207",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m208",function(){
with(this){
return [immediateparent,"width"]
}},"$m209",function($0){
with(this){
var $1=parent.state;if($1!==this["frame"]||!this.inited){
this.setAttribute("frame",$1)
}}},"$m210",function(){
with(this){
return [parent,"state"]
}},"$classrootdepth",void 0],["children",LzNode.mergeChildren([],$lzc$class_resizeview_x["children"]),"attributes",new LzInheritedHash($lzc$class_resizeview_x.attributes)]);Class.make("$lzc$class_m250",LzLayout,["init",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);this.updateDelegate.register(parent,"onwidth")
}},"addSubview",function($0){
if($0.name=="title"){
this.updateDelegate.register($0,"ontext")
}else if($0.name!="gripper"){
this.updateDelegate.register($0,"onwidth")
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["addSubview"]||this.nextMethod(arguments.callee,"addSubview")).call(this,$0)
},"update",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked)return;var $1=15,$2=15;var $3=4;var $4=parent.icon.width>0?parent.icon.width+$3:parent.icon.width;var $5=0;if(parent.title.getText()==""){
parent.gripper_left.setAttribute("width",parent.width-parent.controls.x);parent.gripper_right.setAttribute("width",0);return
}else{
parent.gripper_left.setAttribute("width",$1);$5=parent.title.getTextWidth();$5=Math.ceil($5)
};var $6=$1+$3;parent.icon.setAttribute("x",$6);$6+=$4;parent.title.setAttribute("x",$6);parent.controls.setAttribute("x",parent.width-parent.controls.width);var $7=parent.width-$4-parent.controls.width-$1-$2-$3*2;if($5>$7){
$5=$7
};parent.title.setAttribute("width",$5);$6+=$5+$3;parent.gripper_right.setAttribute("x",$6);parent.gripper_right.setAttribute("width",parent.controls.x-$6)
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzLayout.attributes)]);Class.make("$lzc$class_m251",LzView,["$m217",function($0){
with(this){
var $1=classroot.state;if($1!==this["frame"]||!this.inited){
this.setAttribute("frame",$1)
}}},"$m218",function(){
with(this){
return [classroot,"state"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m252",LzText,["$m219",function($0){
with(this){
var $1=classroot["style"]?classroot.style.textcolor:null;if($1!==this["fgcolor"]||!this.inited){
this.setAttribute("fgcolor",$1)
}}},"$m220",function(){
with(this){
return [this,"classroot",classroot.style,"textcolor"]
}},"$m221",function($0){
with(this){
var $1=classroot.state==2?0.7:1;if($1!==this["opacity"]||!this.inited){
this.setAttribute("opacity",$1)
}}},"$m222",function(){
with(this){
return [classroot,"state"]
}},"$m223",function($0){
with(this){
var $1=classroot.title;if($1!==this["text"]||!this.inited){
this.setAttribute("text",$1)
}}},"$m224",function(){
with(this){
return [classroot,"title"]
}},"$m225",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m226",function(){
with(this){
return [immediateparent,"width"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m253",LzView,["$m227",function($0){
with(this){
var $1=classroot.state;if($1!==this["frame"]||!this.inited){
this.setAttribute("frame",$1)
}}},"$m228",function(){
with(this){
return [classroot,"state"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m254",$lzc$class_basebutton,["$m231",function($0){
with(this){
classroot.close()
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash($lzc$class_basebutton.attributes)]);Class.make("$lzc$class_m249",LzView,["$m211",function($0){
with(this){
var $1=classroot.inset_left;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m212",function(){
with(this){
return [classroot,"inset_left"]
}},"$m213",function($0){
with(this){
var $1=classroot.titlearea_inset_top;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m214",function(){
with(this){
return [classroot,"titlearea_inset_top"]
}},"$m215",function($0){
with(this){
var $1=classroot.width-classroot.inset_left-classroot.inset_right;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m216",function(){
with(this){
return [classroot,"width",classroot,"inset_left",classroot,"inset_right"]
}},"titlelayout",void 0,"gripper_left",void 0,"icon",void 0,"title",void 0,"gripper_right",void 0,"controls",void 0,"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,name:"titlelayout"},"class":$lzc$class_m250},{attrs:{$classrootdepth:2,clip:true,frame:new LzAlwaysExpr("$m217","$m218"),name:"gripper_left",resource:"window_gripper_rsc",width:16,y:1},"class":$lzc$class_m251},{attrs:{$classrootdepth:2,name:"icon"},"class":LzView},{attrs:{$classrootdepth:2,fgcolor:new LzAlwaysExpr("$m219","$m220"),name:"title",opacity:new LzAlwaysExpr("$m221","$m222"),resize:false,text:new LzAlwaysExpr("$m223","$m224"),width:new LzAlwaysExpr("$m225","$m226"),y:-1},"class":$lzc$class_m252},{attrs:{$classrootdepth:2,clip:true,frame:new LzAlwaysExpr("$m227","$m228"),name:"gripper_right",resource:"window_gripper_rsc",y:1},"class":$lzc$class_m253},{attrs:{$classrootdepth:2,close_btn:void 0,closeable:void 0,name:"controls"},children:[{attrs:{$classrootdepth:3,$m229:function($0){
with(this){
var $1=classroot.closeable;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}}},$m230:function(){
with(this){
return [classroot,"closeable"]
}},applied:new LzAlwaysExpr("$m229","$m230"),close_btn:void 0,name:"closeable"},children:[{attrs:{$classrootdepth:3,$delegates:["onclick","$m231",null],clickable:true,name:"close_btn",resource:"window_closebtn_rsc",styleable:true,x:2},"class":$lzc$class_m254}],"class":LzState}],"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m255",LzView,["$m232",function($0){
with(this){
var $1=parent.inset_left;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m233",function(){
with(this){
return [parent,"inset_left"]
}},"$m234",function($0){
with(this){
var $1=parent.inset_top;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m235",function(){
with(this){
return [parent,"inset_top"]
}},"$m236",function($0){
with(this){
var $1=classroot.bgcolor;if($1!==this["bgcolor"]||!this.inited){
this.setAttribute("bgcolor",$1)
}}},"$m237",function(){
with(this){
return [classroot,"bgcolor"]
}},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,$m238:function($0){
with(this){
var $1=!classroot._usecontentwidth;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}}},$m239:function(){
with(this){
return [classroot,"_usecontentwidth"]
}},$m240:function($0){
with(this){
var $1=classroot.width-classroot.inset_left-classroot.inset_right;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},$m241:function(){
with(this){
return [classroot,"width",classroot,"inset_left",classroot,"inset_right"]
}},applied:new LzAlwaysExpr("$m238","$m239"),width:new LzAlwaysExpr("$m240","$m241")},"class":LzState},{attrs:{$classrootdepth:2,$m242:function($0){
with(this){
var $1=!classroot._usecontentheight;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}}},$m243:function(){
with(this){
return [classroot,"_usecontentheight"]
}},$m244:function($0){
with(this){
var $1=classroot.height-classroot.inset_bottom-classroot.inset_top;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},$m245:function(){
with(this){
return [classroot,"height",classroot,"inset_bottom",classroot,"inset_top"]
}},applied:new LzAlwaysExpr("$m242","$m243"),height:new LzAlwaysExpr("$m244","$m245")},"class":LzState}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_windowpanel",$lzc$class_basewindow,["title",void 0,"closeable",void 0,"inset_left",void 0,"inset_top",void 0,"inset_right",void 0,"inset_bottom",void 0,"titlearea_inset_top",void 0,"$lzc$set_bgcolor",function($0){
this.bgcolor=$0;var $1=this["onbgcolor"];if($1&&$1.ready){
$1.sendEvent($0)
}},"_usecontentwidth",void 0,"_usecontentheight",void 0,"construct",function($0,$1){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["construct"]||this.nextMethod(arguments.callee,"construct")).call(this,$0,$1);setAttribute("_usecontentwidth",!hassetwidth);setAttribute("_usecontentheight",!hassetheight)
}},"init",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);checkMinSize()
}},"checkMinSize",function(){
with(this){
if(this.width<this.minwidth){
if(!this._usecontentwidth){
Debug.write("requested width smaller than minwidth,"+" ignored: "+this)
};this.setAttribute("width",this.minwidth)
};if(this.height<this.minheight){
if(!this._usecontentheight){
Debug.write("requested height smaller than minheight,"+" ignored: "+this)
};this.setAttribute("height",this.minheight)
}}},"top",void 0,"middle",void 0,"bottom",void 0,"title_area",void 0,"content",void 0,"_applystyle",function($0){
with(this){
setTint(this.top,$0.basecolor);setTint(this.middle,$0.basecolor);setTint(this.bottom,$0.basecolor);title_area.title.setAttribute("fgcolor",$0.textcolor);if(this.bgcolor==null){
this.content.setAttribute("bgcolor",$0.bgcolor)
}}}],["tagname","windowpanel","children",LzNode.mergeChildren([{attrs:{$classrootdepth:1,$m191:function($0){
with(this){
var $1=parent._usecontentwidth;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}}},$m192:function(){
with(this){
return [parent,"_usecontentwidth"]
}},$m193:function($0){
var $1=this.content.width+this.inset_left+this.inset_right;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}},$m194:function(){
return [this.content,"width",this,"inset_left",this,"inset_right"]
},applied:new LzAlwaysExpr("$m191","$m192"),width:new LzAlwaysExpr("$m193","$m194")},"class":LzState},{attrs:{$classrootdepth:1,$m195:function($0){
with(this){
var $1=parent._usecontentheight;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}}},$m196:function(){
with(this){
return [parent,"_usecontentheight"]
}},$m197:function($0){
var $1=this.content.height+this.inset_top+this.inset_bottom;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}},$m198:function(){
return [this.content,"height",this,"inset_top",this,"inset_bottom"]
},applied:new LzAlwaysExpr("$m195","$m196"),height:new LzAlwaysExpr("$m197","$m198")},"class":LzState},{attrs:{$classrootdepth:1,frame:new LzAlwaysExpr("$m201","$m202"),height:13.9,left:"window_TL_rsc",middle:"window_TM_rsc",name:"top",right:"window_TR_rsc",width:new LzAlwaysExpr("$m199","$m200")},"class":$lzc$class_m246},{attrs:{$classrootdepth:1,frame:new LzAlwaysExpr("$m205","$m206"),left:"window_ML_rsc",middle:"window_MM_rsc",name:"middle",right:"window_MR_rsc",width:new LzAlwaysExpr("$m203","$m204")},"class":$lzc$class_m247},{attrs:{$classrootdepth:1,frame:new LzAlwaysExpr("$m209","$m210"),left:"window_BL_rsc",middle:"window_BM_rsc",name:"bottom",right:"window_BR_rsc",width:new LzAlwaysExpr("$m207","$m208")},"class":$lzc$class_m248},{attrs:{$classrootdepth:1,axis:"y"},"class":$lzc$class_stableborderlayout},{attrs:{$classrootdepth:1,controls:void 0,gripper_left:void 0,gripper_right:void 0,icon:void 0,name:"title_area",title:void 0,titlelayout:void 0,width:new LzAlwaysExpr("$m215","$m216"),x:new LzAlwaysExpr("$m211","$m212"),y:new LzAlwaysExpr("$m213","$m214")},"class":$lzc$class_m249},{attrs:{$classrootdepth:1,bgcolor:new LzAlwaysExpr("$m236","$m237"),clip:true,name:"content",x:new LzAlwaysExpr("$m232","$m233"),y:new LzAlwaysExpr("$m234","$m235")},"class":$lzc$class_m255},{attrs:"content","class":$lzc$class_userClassPlacement}],$lzc$class_basewindow["children"]),"attributes",new LzInheritedHash($lzc$class_basewindow.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_usecontentheight:true,_usecontentwidth:true,closeable:false,inset_bottom:20,inset_left:6,inset_right:11,inset_top:22,title:"",titlearea_inset_top:6},$lzc$class_windowpanel.attributes)
}}})($lzc$class_windowpanel);Class.make("$lzc$class_simplelayout",LzLayout,["axis",void 0,"$lzc$set_axis",function($0){
this.setAxis($0)
},"inset",void 0,"$lzc$set_inset",function($0){
this.inset=$0;if(this.subviews&&this.subviews.length)this.update();if(this["oninset"])this.oninset.sendEvent(this.inset)
},"spacing",void 0,"$lzc$set_spacing",function($0){
this.spacing=$0;if(this.subviews&&this.subviews.length)this.update();if(this["onspacing"])this.onspacing.sendEvent(this.spacing)
},"setAxis",function($0){
if(this["axis"]==null||this.axis!=$0){
this.axis=$0;this.sizeAxis=$0=="x"?"width":"height";if(this.subviews.length)this.update();if(this["onaxis"])this.onaxis.sendEvent(this.axis)
}},"addSubview",function($0){
this.updateDelegate.register($0,"on"+this.sizeAxis);this.updateDelegate.register($0,"onvisible");if(!this.locked){
var $1=null;var $2=this.subviews;for(var $3=$2.length-1;$3>=0;--$3){
if($2[$3].visible){
$1=$2[$3];break
}};if($1){
var $4=$1[this.axis]+$1[this.sizeAxis]+this.spacing
}else{
var $4=this.inset
};$0.setAttribute(this.axis,$4)
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["addSubview"]||this.nextMethod(arguments.callee,"addSubview")).call(this,$0)
},"update",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked)return;var $1=this.subviews.length;var $2=this.inset;for(var $3=0;$3<$1;$3++){
var $4=this.subviews[$3];if(!$4.visible)continue;if($4[this.axis]!=$2){
$4.setAttribute(this.axis,$2)
};if($4.usegetbounds){
$4=$4.getBounds()
};$2+=this.spacing+$4[this.sizeAxis]
}}],["tagname","simplelayout","attributes",new LzInheritedHash(LzLayout.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({axis:"y",inset:0,spacing:0},$lzc$class_simplelayout.attributes)
}}})($lzc$class_simplelayout);Class.make("$lzc$class_resizelayout",LzLayout,["axis",void 0,"$lzc$set_axis",function($0){
this.setAxis($0)
},"spacing",void 0,"construct",function($0,$1){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["construct"]||this.nextMethod(arguments.callee,"construct")).call(this,$0,$1);this.resetDelegate=new LzDelegate(this,"reset");this.heldSubs=new Object();if($1["releaseview"]!=null){
this.heldSubs[$0[$1.releaseview].getUID()]=false
};if($1["release"]!=null){
this.heldSubs[$1.release.getUID()]=false
};this.delegates.push(this.resetDelegate)
}},"setAxis",function($0){
this.axis=$0;this.sizeAxis=$0=="x"?"width":"height";this.updateDelegate.register(this.immediateparent,"on"+this.sizeAxis)
},"addSubview",function($0){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["addSubview"]||this.nextMethod(arguments.callee,"addSubview")).call(this,$0);var $1=$0.getUID();if($0.getOption("releasetolayout")){
$0.setAttribute(this.sizeAxis,0)
}else{
this.hold($0)
};this.resetDelegate.register($0,"onvisible");this.resetDelegate.register($0,"on"+this.sizeAxis);this.reset()
},"hold",function($0){
this.heldSubs[$0.getUID()]=true;this.reset()
},"release",function($0){
this.heldSubs[$0.getUID()]=false;this.reset()
},"reset",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked){
return
};var $1=this.subviews.length;this.totalHeld=0;this.heldAmount=0;this.lastUnheld=-1;var $2=0;for(var $3=0;$3<$1;$3++){
var $4=this.subviews[$3];if($4.visible)$2++
};var $5=0;for(var $3=0;$3<$1;$3++){
var $4=this.subviews[$3];if(!$4.visible)continue;var $6=$5<$2-1?this.spacing:0;if(this.heldSubs[$4.getUID()]){
$6+=$4[this.sizeAxis];this.heldAmount+=$6;this.totalHeld++
}else{
this.lastUnheld=$3;this.heldAmount+=$6
};$5++
};this.unheldCount=$2-this.totalHeld;this.update()
},"update",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked){
return
};this.lock();var $1=this.immediateparent[this.sizeAxis];var $2=$1-this.heldAmount;var $3=$2/this.unheldCount;var $4=this.subviews.length;var $5=0;for(var $6=0;$6<$4;$6++){
var $7=this.subviews[$6];if(!$7.visible)continue;$7.setAttribute(this.axis,$5);if(!this.heldSubs[$7.getUID()]){
$7.setAttribute(this.sizeAxis,$3)
};$5+=this.spacing+$7[this.sizeAxis]
};this.locked=false
}],["tagname","resizelayout","attributes",new LzInheritedHash(LzLayout.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({axis:"y",spacing:0},$lzc$class_resizelayout.attributes)
}}})($lzc$class_resizelayout);Class.make("$lzc$class_m277",LzView,["$m264",function($0){
with(this){
var $1=classroot.content_inset_left;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m265",function(){
with(this){
return [classroot,"content_inset_left"]
}},"$m266",function($0){
with(this){
var $1=classroot.content_inset_top;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m267",function(){
with(this){
return [classroot,"content_inset_top"]
}},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:3,$m268:function($0){
with(this){
var $1=!classroot._usecontentwidth;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}}},$m269:function(){
with(this){
return [classroot,"_usecontentwidth"]
}},$m270:function($0){
with(this){
var $1=parent.immediateparent.width-classroot.content_inset_left-classroot.content_inset_right;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},$m271:function(){
with(this){
return [parent.immediateparent,"width",classroot,"content_inset_left",classroot,"content_inset_right"]
}},applied:new LzAlwaysExpr("$m268","$m269"),width:new LzAlwaysExpr("$m270","$m271")},"class":LzState},{attrs:{$classrootdepth:3,$m272:function($0){
with(this){
var $1=!classroot._usecontentheight;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}}},$m273:function(){
with(this){
return [classroot,"_usecontentheight"]
}},$m274:function($0){
with(this){
var $1=parent.immediateparent.height-classroot.content_inset_top-classroot.content_inset_bottom;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},$m275:function(){
with(this){
return [parent.immediateparent,"height",classroot,"content_inset_top",classroot,"content_inset_bottom"]
}},applied:new LzAlwaysExpr("$m272","$m273"),height:new LzAlwaysExpr("$m274","$m275")},"class":LzState}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m276",LzView,["$m260",function($0){
with(this){
var $1=mdcontent.width+parent.content_inset_left+parent.content_inset_right;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m261",function(){
with(this){
return [mdcontent,"width",parent,"content_inset_left",parent,"content_inset_right"]
}},"$m262",function($0){
with(this){
var $1=mdcontent.height+parent.content_inset_top+parent.content_inset_bottom;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m263",function(){
with(this){
return [mdcontent,"height",parent,"content_inset_top",parent,"content_inset_bottom"]
}},"mdcontent",void 0,"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,name:"mdcontent",x:new LzAlwaysExpr("$m264","$m265"),y:new LzAlwaysExpr("$m266","$m267")},"class":$lzc$class_m277}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_modaldialog",$lzc$class_windowpanel,["$m256",function($0){
with(this){
var $1=(immediateparent.width-this.width)/2;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m257",function(){
with(this){
return [immediateparent,"width",this,"width"]
}},"$m258",function($0){
with(this){
var $1=(immediateparent.height-this.height)/3;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m259",function(){
with(this){
return [immediateparent,"height",this,"height"]
}},"content_inset_left",void 0,"content_inset_top",void 0,"content_inset_right",void 0,"content_inset_bottom",void 0,"mdpadding",void 0,"open",function(){
with(this){
this.setAttribute("visible",true);lz.ModeManager.makeModal(this);this.bringToFront()
}},"close",function(){
with(this){
var $0=Array.prototype.slice.call(arguments,0);this.setAttribute("visible",false);lz.Focus.clearFocus();lz.ModeManager.release(this)
}},"passModeEvent",function($0,$1){
return false
}],["tagname","modaldialog","children",LzNode.mergeChildren([{attrs:{$classrootdepth:1,height:new LzAlwaysExpr("$m262","$m263"),mdcontent:void 0,name:"mdpadding",width:new LzAlwaysExpr("$m260","$m261")},"class":$lzc$class_m276},{attrs:"mdcontent","class":$lzc$class_userClassPlacement}],$lzc$class_windowpanel["children"]),"attributes",new LzInheritedHash($lzc$class_windowpanel.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({content_inset_bottom:10,content_inset_left:14,content_inset_right:14,content_inset_top:10,focustrap:true,visible:false,x:new LzAlwaysExpr("$m256","$m257"),y:new LzAlwaysExpr("$m258","$m259")},$lzc$class_modaldialog.attributes)
}}})($lzc$class_modaldialog);Class.make("$lzc$class_m301",LzText,["$m281",function($0){
with(this){
var $1=parent.text_x;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m282",function(){
with(this){
return [parent,"text_x"]
}},"$m283",function($0){
with(this){
var $1=parent.text_y;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m284",function(){
with(this){
return [parent,"text_y"]
}},"$m285",function($0){
with(this){
var $1=parent.text;if($1!==this["text"]||!this.inited){
this.setAttribute("text",$1)
}}},"$m286",function(){
with(this){
return [parent,"text"]
}},"$lzc$set_text",function($0){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_text"]||this.nextMethod(arguments.callee,"$lzc$set_text")).call(this,$0);if(!parent._usecontentwidth){
this.setAttribute("width",parent.width-parent.inset_left-parent.inset_right-parent.content_inset_left-parent.content_inset_right)
}else{
var $1=this.getTextWidth();if($1>parent.maxtextwidth){
$1=parent.maxtextwidth
};this.setAttribute("width",$1)
};parent.checkMinSize()
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m303",$lzc$class_button,["$m291",function($0){
with(this){
classroot.close(false)
}},"$m292",function($0){
with(this){
var $1=classroot.button2;if($1!==this["text"]||!this.inited){
this.setAttribute("text",$1)
}}},"$m293",function(){
with(this){
return [classroot,"button2"]
}},"$m294",function($0){
with(this){
var $1=classroot.button2!="";if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m295",function(){
with(this){
return [classroot,"button2"]
}},"$classrootdepth",void 0],["children",LzNode.mergeChildren([],$lzc$class_button["children"]),"attributes",new LzInheritedHash($lzc$class_button.attributes)]);Class.make("$lzc$class_m304",$lzc$class_button,["$m296",function($0){
with(this){
classroot.close(true)
}},"$m297",function($0){
with(this){
var $1=classroot.button1;if($1!==this["text"]||!this.inited){
this.setAttribute("text",$1)
}}},"$m298",function(){
with(this){
return [classroot,"button1"]
}},"$m299",function($0){
with(this){
var $1=classroot.button1!="";if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m300",function(){
with(this){
return [classroot,"button1"]
}},"$classrootdepth",void 0],["children",LzNode.mergeChildren([],$lzc$class_button["children"]),"attributes",new LzInheritedHash($lzc$class_button.attributes)]);Class.make("$lzc$class_m302",LzView,["$m287",function($0){
with(this){
var $1=immediateparent.width>this.width?immediateparent.width-this.width:0;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m288",function(){
with(this){
return [immediateparent,"width",this,"width"]
}},"$m289",function($0){
with(this){
var $1=immediateparent.alerttext.height+parent.content_inset_top;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m290",function(){
with(this){
return [immediateparent.alerttext,"height",parent,"content_inset_top"]
}},"b1",void 0,"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,axis:"x",spacing:5},"class":$lzc$class_simplelayout},{attrs:{$classrootdepth:2,$delegates:["onclick","$m291",null],clickable:true,text:new LzAlwaysExpr("$m292","$m293"),visible:new LzAlwaysExpr("$m294","$m295")},"class":$lzc$class_m303},{attrs:{$classrootdepth:2,$delegates:["onclick","$m296",null],clickable:true,isdefault:true,name:"b1",text:new LzAlwaysExpr("$m297","$m298"),visible:new LzAlwaysExpr("$m299","$m300")},"class":$lzc$class_m304}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_alert",$lzc$class_modaldialog,["button1",void 0,"button2",void 0,"result",void 0,"text_x",void 0,"text_y",void 0,"$m278",function($0){
with(this){
this.setAttribute("minwidth",button2==""?100:170)
}},"onresult",void 0,"$m279",function($0){
with(this){
var $1=Math.round(parent.width/3)-inset_left-inset_right-content_inset_left-content_inset_right;if($1!==this["maxtextwidth"]||!this.inited){
this.setAttribute("maxtextwidth",$1)
}}},"$m280",function(){
with(this){
return [parent,"width",this,"inset_left",this,"inset_right",this,"content_inset_left",this,"content_inset_right"].concat(Math["$lzc$round_dependencies"]?Math["$lzc$round_dependencies"](this,Math,parent.width/3):[])
}},"maxtextwidth",void 0,"alerttext",void 0,"open",function(){
this.result=null;if(this.onresult){
this.onresult.sendEvent(null)
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["open"]||this.nextMethod(arguments.callee,"open")).call(this)
},"close",function(){
with(this){
var $0=Array.prototype.slice.call(arguments,0);var $1=$0[0];this.result=$1;if(this.onresult){
this.onresult.sendEvent($1)
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["close"]||this.nextMethod(arguments.callee,"close")).call(this)
}}],["tagname","alert","children",LzNode.mergeChildren([{attrs:{$classrootdepth:1,multiline:true,name:"alerttext",resize:true,text:new LzAlwaysExpr("$m285","$m286"),x:new LzAlwaysExpr("$m281","$m282"),y:new LzAlwaysExpr("$m283","$m284")},"class":$lzc$class_m301},{attrs:{$classrootdepth:1,b1:void 0,x:new LzAlwaysExpr("$m287","$m288"),y:new LzAlwaysExpr("$m289","$m290")},"class":$lzc$class_m302}],$lzc$class_modaldialog["children"]),"attributes",new LzInheritedHash($lzc$class_modaldialog.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({button1:"OK",button2:"",maxtextwidth:new LzAlwaysExpr("$m279","$m280"),minwidth:new LzOnceExpr("$m278"),onresult:LzDeclaredEvent,result:null,text_x:0,text_y:0},$lzc$class_alert.attributes)
}}})($lzc$class_alert);Class.make("$lzc$class_m336",LzView,["$m307",function($0){
with(this){
var $1=classroot.width-this.width-classroot.inset_right;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m308",function(){
with(this){
return [classroot,"width",this,"width",classroot,"inset_right"]
}},"$m309",function($0){
with(this){
var $1=classroot.height-this.height-classroot.inset_right;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m310",function(){
with(this){
return [classroot,"height",this,"height",classroot,"inset_right"]
}},"$m311",function($0){
with(this){
if(this.frame==1)setAttribute("frame",2)
}},"$m312",function($0){
with(this){
if(this.frame==2)setAttribute("frame",1)
}},"$m313",function($0){
with(this){
classroot._startResize();setAttribute("frame",3)
}},"$m314",function($0){
with(this){
classroot._stopResize();setAttribute("frame",1)
}},"$m315",function($0){
with(this){
classroot.setTint(this,classroot.style.basecolor)
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m337",LzView,["$m316",function($0){
with(this){
var $1=parent.wframe.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m317",function(){
with(this){
return [parent.wframe,"width"]
}},"$m318",function($0){
with(this){
classroot.menubar=this
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m338",LzView,["$m319",function($0){
with(this){
var $1=parent.wframe.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m320",function(){
with(this){
return [parent.wframe,"width"]
}},"$m321",function($0){
with(this){
classroot.toolbar=this
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m340",LzView,["$m326",function($0){
with(this){
var $1=classroot.content.bgcolor;if($1!==this["bgcolor"]||!this.inited){
this.setAttribute("bgcolor",$1)
}}},"$m327",function(){
with(this){
return [classroot.content,"bgcolor"]
}},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:3,$m328:function($0){
with(this){
var $1=!classroot._usecontentwidth;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}}},$m329:function(){
with(this){
return [classroot,"_usecontentwidth"]
}},$m330:function($0){
with(this){
var $1=parent.immediateparent.width-2;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},$m331:function(){
with(this){
return [parent.immediateparent,"width"]
}},applied:new LzAlwaysExpr("$m328","$m329"),width:new LzAlwaysExpr("$m330","$m331")},"class":LzState},{attrs:{$classrootdepth:3,$m332:function($0){
with(this){
var $1=!classroot._usecontentheight;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}}},$m333:function(){
with(this){
return [classroot,"_usecontentheight"]
}},$m334:function($0){
with(this){
var $1=parent.immediateparent.height-parent.immediateparent.menubar.height-parent.immediateparent.toolbar.height-2;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},$m335:function(){
with(this){
return [parent.immediateparent,"height",parent.immediateparent.menubar,"height",parent.immediateparent.toolbar,"height"]
}},applied:new LzAlwaysExpr("$m332","$m333"),height:new LzAlwaysExpr("$m334","$m335")},"class":LzState}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m339",LzView,["$m322",function($0){
with(this){
var $1=wcontent.width+2;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m323",function(){
with(this){
return [wcontent,"width"]
}},"$m324",function($0){
with(this){
var $1=wcontent.height+2;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m325",function(){
with(this){
return [wcontent,"height"]
}},"wcontent",void 0,"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,bgcolor:new LzAlwaysExpr("$m326","$m327"),clip:true,name:"wcontent",x:1,y:1},"class":$lzc$class_m340}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_window",$lzc$class_windowpanel,["resizable",void 0,"_startResize",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_startResize"]||this.nextMethod(arguments.callee,"_startResize")).call(this);setAttribute("_usecontentwidth",false);setAttribute("_usecontentheight",false)
}},"resizeable",void 0,"menubar",void 0,"toolbar",void 0,"wframe",void 0,"_resizeControl",void 0,"_applystyle",function($0){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_applystyle"]||this.nextMethod(arguments.callee,"_applystyle")).call(this,$0);if(this._resizeControl){
this.setTint(this._resizeControl,$0.basecolor)
}}],["tagname","window","children",LzNode.mergeChildren([{attrs:{$classrootdepth:1,$m305:function($0){
var $1=this.classroot.resizable;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}},$m306:function(){
return [this.classroot,"resizable"]
},_resizeControl:void 0,applied:new LzAlwaysExpr("$m305","$m306"),name:"resizeable"},children:[{attrs:{$classrootdepth:1,$delegates:["onmouseover","$m311",null,"onmouseout","$m312",null,"onmousedown","$m313",null,"onmouseup","$m314",null,"oninit","$m315",null],clickable:true,name:"_resizeControl",placement:"null",resource:"window_resizebtn_rsc",x:new LzAlwaysExpr("$m307","$m308"),y:new LzAlwaysExpr("$m309","$m310")},"class":$lzc$class_m336}],"class":LzState},{attrs:{$classrootdepth:1,$delegates:["oninit","$m318",null],clip:true,name:"menubar",width:new LzAlwaysExpr("$m316","$m317")},"class":$lzc$class_m337},{attrs:{$classrootdepth:1,$delegates:["oninit","$m321",null],clip:true,name:"toolbar",width:new LzAlwaysExpr("$m319","$m320")},"class":$lzc$class_m338},{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0x6a6a6a"),height:new LzAlwaysExpr("$m324","$m325"),name:"wframe",options:{releasetolayout:true},wcontent:void 0,width:new LzAlwaysExpr("$m322","$m323")},"class":$lzc$class_m339},{attrs:{$classrootdepth:1,axis:"y"},"class":$lzc$class_resizelayout},{attrs:"wcontent","class":$lzc$class_userClassPlacement}],$lzc$class_windowpanel["children"]),"attributes",new LzInheritedHash($lzc$class_windowpanel.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_resizeControl:null,resizable:false},$lzc$class_window.attributes)
}}})($lzc$class_window);Class.make("$lzc$class_pivotlayout",LzLayout,["axis",void 0,"$lzc$set_axis",function($0){
this.setAxis($0)
},"spacing",void 0,"xinset",void 0,"yinset",void 0,"dimmer",void 0,"$m441",function($0){
var $1=this.spacing;if($1!==this["xspacing"]||!this.inited){
this.setAttribute("xspacing",$1)
}},"$m442",function(){
return [this,"spacing"]
},"xspacing",void 0,"$m443",function($0){
var $1=this.spacing;if($1!==this["yspacing"]||!this.inited){
this.setAttribute("yspacing",$1)
}},"$m444",function(){
return [this,"spacing"]
},"yspacing",void 0,"photoscale",void 0,"photodimension",void 0,"skewindex",void 0,"isgrid",void 0,"textvisible",void 0,"skew",void 0,"pivotrow",void 0,"pivotindex",void 0,"pivot_x",void 0,"pivot_y",void 0,"startpivot_x",void 0,"startpivot_y",void 0,"pagebegin",void 0,"pageend",void 0,"pagesize",void 0,"totalitems",void 0,"currentpage",void 0,"perpage",void 0,"totalpages",void 0,"duration",void 0,"calcpageparams",void 0,"setAxis",function($0){
this.axis=$0;this.otherAxis=$0=="x"?"y":"x";this.sizeAxis=$0=="x"?"width":"height";this.otherSizeAxis=$0=="x"?"height":"width"
},"$m445",function($0){
this.regUpdateDelegate()
},"regUpdateDelegate",function(){
this.updateDelegate.register(this.immediateparent,"onwidth");this.updateDelegate.register(this,"onphotodimension");this.updateDelegate.register(this,"onphotoscale");this.updateDelegate.register(this,"onyspacing");this.updateDelegate.register(this,"onxspacing");this.updateDelegate.register(this,"onpivotindex");this.updateDelegate.register(this,"onskew");this.updateDelegate.register(this,"onpivot_x");this.updateDelegate.register(this,"onpivot_y");this.updateDelegate.register(this,"ondimmer")
},"$m446",function($0){
with(this){
this.setAttribute("coidel",new LzDelegate(this,"update"))
}},"coidel",void 0,"updateOnIdle",function($0){
with(this){
if($0){
this.updateDelegate.unregisterAll();coidel.register(lz.Idle,"onidle")
}else{
this.regUpdateDelegate();coidel.unregisterAll()
}}},"update",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked)return;this.locked=true;var $1=this.immediateparent.width;var $2=Math.round(photodimension*photoscale);var $3=Math.floor($1/($2+this.xspacing));$1=$3*($2+this.xspacing);var $4=this.immediateparent.height;if(this.isgrid){
var $5=Math.floor($4/($2+this.xspacing))
}else var $5=1;this.setAttribute("totalitems",$3*$5);var $6=Math.floor(this.pivotindex/$3);var $7=this.pivot_x-this.pivotindex%$3*($2+this.xspacing);var $8=0;var $9=($8-$6)*this.skew*$1+$7;var $a=$9+$1;var $b=$9;var $c=this.pivot_y-($2+this.yspacing)*$6+this.yinset;var $d=-1;var $e=-1;var $f=this.subviews.length;for(var $g=0;$g<$f;$g++){
var $h=this.subviews[$g];$h.index=$g;if(!$h.intparent)continue;if($g!=pivotindex){
$h.intparent.setAttribute("opacity",dimmer)
};$h.setAttribute("x",$b);$h.setAttribute("y",$c);if($h.txt){
$h.txt.setAttribute("visible",this.textvisible)
};$h.setAttribute("width",$2);$h.setAttribute("height",$2);if(calcpageparams){
var $i=$h.x+$h.width<0;$i=$i||$h.x>this.immediateparent.width;$i=$i||$h.y+$h.height>this.immediateparent.height;if($i){
$h.setAttribute("visible",false);if($e==-1&&$d>-1)$e=$g-1
}else{
$h.setAttribute("visible",true);if($d==-1)$d=$g
}};$b+=$2;if($g<$f-1){
$b+=xspacing;if($b>$a||$b+$2>$a){
$8+=1;$9=($8-$6)*skew*$1+$7;$a=$9+$1;$b=$9;$c+=$2+yspacing
}}};if(calcpageparams){
if($e==-1)$e=$f-1;this.setAttribute("pagebegin",$d);this.setAttribute("pageend",$e);this.setAttribute("pagesize",$e-$d+1)
};this.locked=false
}},"toString",function(){
return "pivotlayout for "+this.immediateparent
},"dim",function($0){
with(this){
var $1=this.subviews.length;for(var $2=0;$2<$1;$2++){
var $3;if($2!=pivotindex){
$3=this.subviews[$2];$3.intparent.setAttribute("opacity",$0)
}}}}],["tagname","pivotlayout","attributes",new LzInheritedHash(LzLayout.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["oninit","$m445",null],axis:"x",calcpageparams:true,coidel:new LzOnceExpr("$m446"),currentpage:1,dimmer:1,duration:0,isgrid:true,pagebegin:-1,pageend:-1,pagesize:0,perpage:0,photodimension:70,photoscale:1,pivot_x:0,pivot_y:0,pivotindex:0,pivotrow:0,skew:0,skewindex:1,spacing:1,startpivot_x:50,startpivot_y:50,textvisible:true,totalitems:0,totalpages:0,xinset:0,xspacing:new LzAlwaysExpr("$m441","$m442"),yinset:0,yspacing:new LzAlwaysExpr("$m443","$m444")},$lzc$class_pivotlayout.attributes)
}}})($lzc$class_pivotlayout);Class.make("$lzc$class_m448",LzAnimator,["$m354",function($0){
with(this){
var $1=classroot.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m355",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m449",LzAnimator,["$m356",function($0){
with(this){
var $1=classroot.anm_multipler*450;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m357",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m450",LzAnimator,["$m358",function($0){
with(this){
var $1=classroot.anm_multipler*550;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m359",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m452",LzAnimator,["$m361",function($0){
with(this){
var $1=classroot.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m362",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m453",LzAnimator,["$m363",function($0){
with(this){
var $1=classroot.height-150;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m364",function(){
with(this){
return [classroot,"height"]
}},"$m365",function($0){
with(this){
var $1=classroot.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m366",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m454",LzAnimator,["$m367",function($0){
with(this){
var $1=classroot.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m368",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m455",LzAnimator,["$m369",function($0){
with(this){
var $1=classroot.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m370",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m456",LzAnimator,["$m371",function($0){
with(this){
var $1=classroot.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m372",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m451",LzAnimatorGroup,["$m360",function($0){
with(this){
classroot.details.ph.posme.doStart()
}},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:5,attribute:"pivot_x",duration:new LzAlwaysExpr("$m361","$m362"),relative:true,to:-40},"class":$lzc$class_m452},{attrs:{$classrootdepth:5,attribute:"pivot_y",duration:new LzAlwaysExpr("$m365","$m366"),to:new LzAlwaysExpr("$m363","$m364")},"class":$lzc$class_m453},{attrs:{$classrootdepth:5,attribute:"photodimension",duration:new LzAlwaysExpr("$m367","$m368"),to:50},"class":$lzc$class_m454},{attrs:{$classrootdepth:5,attribute:"photoscale",duration:new LzAlwaysExpr("$m369","$m370"),to:1},"class":$lzc$class_m455},{attrs:{$classrootdepth:5,attribute:"yspacing",duration:new LzAlwaysExpr("$m371","$m372"),to:-50},"class":$lzc$class_m456}],"attributes",new LzInheritedHash(LzAnimatorGroup.attributes)]);Class.make("$lzc$class_m458",LzAnimator,["$m375",function($0){
with(this){
var $1=classroot.details.title;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m376",function(){
with(this){
return [classroot.details,"title"]
}},"$m377",function($0){
with(this){
var $1=classroot.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m378",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m459",LzAnimator,["$m379",function($0){
with(this){
var $1=classroot.details.info;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m380",function(){
with(this){
return [classroot.details,"info"]
}},"$m381",function($0){
with(this){
var $1=classroot.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m382",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m457",LzAnimatorGroup,["$m373",function($0){
with(this){
var $1=classroot.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m374",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:5,attribute:"x",duration:new LzAlwaysExpr("$m377","$m378"),target:new LzAlwaysExpr("$m375","$m376"),to:40},"class":$lzc$class_m458},{attrs:{$classrootdepth:5,attribute:"x",duration:new LzAlwaysExpr("$m381","$m382"),target:new LzAlwaysExpr("$m379","$m380"),to:45},"class":$lzc$class_m459}],"attributes",new LzInheritedHash(LzAnimatorGroup.attributes)]);Class.make("$lzc$class_m461",LzAnimator,["$m387",function($0){
with(this){
this.setAttribute("target",tls)
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m462",LzAnimator,["$m388",function($0){
with(this){
var $1=-classroot.width;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m389",function(){
with(this){
return [classroot,"width"]
}},"$m390",function($0){
with(this){
var $1=classroot.details.title;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m391",function(){
with(this){
return [classroot.details,"title"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m463",LzAnimator,["$m392",function($0){
with(this){
var $1=classroot.width;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m393",function(){
with(this){
return [classroot,"width"]
}},"$m394",function($0){
with(this){
var $1=classroot.details.info;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m395",function(){
with(this){
return [classroot.details,"info"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m460",LzAnimatorGroup,["$m385",function($0){
with(this){
var $1=classroot.anm_multipler*300;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m386",function(){
with(this){
return [classroot,"anm_multipler"]
}},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:4,attribute:"opacity",target:new LzOnceExpr("$m387"),to:1},"class":$lzc$class_m461},{attrs:{$classrootdepth:4,attribute:"x",target:new LzAlwaysExpr("$m390","$m391"),to:new LzAlwaysExpr("$m388","$m389")},"class":$lzc$class_m462},{attrs:{$classrootdepth:4,attribute:"x",target:new LzAlwaysExpr("$m394","$m395"),to:new LzAlwaysExpr("$m392","$m393")},"class":$lzc$class_m463}],"attributes",new LzInheritedHash(LzAnimatorGroup.attributes)]);Class.make("$lzc$class_m464",LzAnimator,["$m396",function($0){
with(this){
var $1=-classroot.width;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m397",function(){
with(this){
return [classroot,"width"]
}},"$m398",function($0){
with(this){
gDebug(this,"onstart","pageNext")
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m465",LzAnimator,["$m399",function($0){
with(this){
var $1=classroot.width;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m400",function(){
with(this){
return [classroot,"width"]
}},"$m401",function($0){
with(this){
gDebug(this,"onstart","pagePrex")
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m447",$lzc$class_pivotlayout,["$m352",function($0){
with(this){
var $1=classroot.tools.zoomscale;if($1!==this["photoscale"]||!this.inited){
this.setAttribute("photoscale",$1)
}}},"$m353",function(){
with(this){
return [classroot.tools,"zoomscale"]
}},"myreset",function(){
with(this){
if(isgrid){
this.photoscale=classroot.tools.zoomscale;this.photodimension=70;this.spacing=50;this.skew=0;this.pivot_x=50;yinset=50;xinset=0;this.pivot_y=0;this.yspacing=50;this.xspacing=50;this.pivotindex=0
}else{
this.photodimension=50;this.photoscale=1;this.skew=1;this.pivot_y=405;this.yspacing=-50;this.xspacing=10;pivotindex=0
};this.update()
}},"transitiontolinear_anm",void 0,"$m383",function(){
with(this){
var $0=transitiontolinear_anm;return $0
}},"$m384",function($0){
this.animate("dimmer",1,500);this.updateOnIdle(false)
},"transitiontogrid_anm",void 0,"pageNext",void 0,"pagePrev",void 0,"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:3,grp:void 0,name:"transitiontolinear_anm",start:false},children:[{attrs:{$classrootdepth:4,name:"grp",process:"simultaneous",yspacing:void 0},children:[{attrs:{$classrootdepth:5,attribute:"skew",duration:new LzAlwaysExpr("$m354","$m355"),from:0,motion:"easeout",to:1},"class":$lzc$class_m448},{attrs:{$classrootdepth:5,attribute:"yspacing",duration:new LzAlwaysExpr("$m356","$m357"),name:"yspacing",to:-70},"class":$lzc$class_m449},{attrs:{$classrootdepth:5,attribute:"xspacing",duration:new LzAlwaysExpr("$m358","$m359"),to:11},"class":$lzc$class_m450}],"class":LzAnimatorGroup},{attrs:{$classrootdepth:4,$delegates:["onstop","$m360",null],process:"simultaneous"},"class":$lzc$class_m451},{attrs:{$classrootdepth:4,duration:new LzAlwaysExpr("$m373","$m374"),process:"simultaneous"},"class":$lzc$class_m457}],"class":LzAnimatorGroup},{attrs:{$classrootdepth:3,duration:new LzAlwaysExpr("$m385","$m386"),name:"transitiontogrid_anm",process:"simultaneous",start:false},"class":$lzc$class_m460},{attrs:{$classrootdepth:3,$delegates:["onstart","$m398",null],attribute:"pivot_x",duration:"1000",name:"pageNext",start:false,to:new LzAlwaysExpr("$m396","$m397")},"class":$lzc$class_m464},{attrs:{$classrootdepth:3,$delegates:["onstart","$m401",null],attribute:"pivot_x",duration:"1000",name:"pagePrev",start:false,to:new LzAlwaysExpr("$m399","$m400")},"class":$lzc$class_m465}],"attributes",new LzInheritedHash($lzc$class_pivotlayout.attributes)]);Class.make("$lzc$class_m466",LzSelectionManager,["isMultiSelect",function($0){
with(this){
return lz.Keys.isKeyDown("control")||lz.Keys.isKeyDown("shift")||parent.isRectangleSelecting
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzSelectionManager.attributes)]);Class.make("$lzc$class_m490",LzView,["$m480",function($0){
with(this){
this.setAttribute("x",classroot.border)
}},"$m481",function($0){
with(this){
this.setAttribute("y",classroot.border)
}},"interior",void 0,"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0xd9d9d9"),name:"interior",stretches:"both"},children:[{attrs:{$classrootdepth:3,$delegates:["onmousedown","$m484",null,"onmouseup","$m485",null],$m482:function($0){
var $1=this.classroot.doesdrag;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}},$m483:function(){
return [this.classroot,"doesdrag"]
},$m484:function($0){
this.classroot.onmousedown.sendEvent()
},$m485:function($0){
this.classroot.onmouseup.sendEvent()
},applied:new LzAlwaysExpr("$m482","$m483"),clickable:true},"class":LzState}],"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m491",LzText,["$m486",function($0){
with(this){
var $1=classroot.height+3;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m487",function(){
with(this){
return [classroot,"height"]
}},"$m488",function($0){
this.adjustDimensions()
},"$m489",function($0){
this.adjustDimensions()
},"adjustDimensions",function(){
with(this){
this.setAttribute("width",classroot.width+20);var $0=Math.min(this.getTextWidth(),width);var $1=(classroot.width-$0)/2;this.setAttribute("x",$1)
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_photo",LzView,["$m467",function($0){
with(this){
var $1=!tls.waitforload;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m468",function(){
with(this){
return [tls,"waitforload"]
}},"title",void 0,"text",void 0,"clipfactor",void 0,"$lzc$set_clipfactor",function($0){
this.setClipFactor($0)
},"loaded",void 0,"aspect",void 0,"_wmult",void 0,"_hmult",void 0,"_iwmult",void 0,"_ihmult",void 0,"_diwmult",void 0,"_dihmult",void 0,"_mdtime",void 0,"_ddcxp",void 0,"_ddcyp",void 0,"$m469",function($0){
var $1=this._wmult*this.width;if($1!==this["intwidth"]||!this.inited){
this.setAttribute("intwidth",$1)
}},"$m470",function(){
return [this,"_wmult",this,"width"]
},"intwidth",void 0,"$m471",function($0){
var $1=this._hmult*this.height;if($1!==this["intheight"]||!this.inited){
this.setAttribute("intheight",$1)
}},"$m472",function(){
return [this,"_hmult",this,"height"]
},"intheight",void 0,"_lastw",void 0,"_lasth",void 0,"border",void 0,"onplainclick",void 0,"doesdrag",void 0,"draginitiator",void 0,"$m473",function($0){
with(this){
this.setAttribute("ddcdel",new LzDelegate(this,"doDragCheck"))
}},"ddcdel",void 0,"mousedownBeforeDrag",void 0,"$m474",function($0){
if(this["txt"]){
if(this.txt.visible)this.txt.adjustDimensions()
}},"$m475",function($0){
if(this.doesdrag)this.startDragCheck()
},"$m476",function($0){
if(this.draginitiator){
this.stopDrag()
}else{
this.ddcdel.unregisterAll();if(this.onplainclick)this.onplainclick.sendEvent(this)
}},"$m477",function($0){
with(this){
if(!$0)return;var $1=this.datapath;this.txt.setAttribute("text",$1.xpathQuery("title/text()"));this.setAttribute("title",$1.xpathQuery("title/text()"));this.setAttribute("text",$1.xpathQuery("description/text()"));this.setImage(this.getImageURL("t"));photoscontainer.lyt.update()
}},"$m478",function(){
with(this){
var $0=intparent.interior;return $0
}},"$m479",function($0){
with(this){
var $1=intparent.interior.resourcewidth;var $2=intparent.interior.resourceheight;if($1>$2){
this.setAttribute("_wmult",1);this.setAttribute("_hmult",$2/$1);this.setAttribute("_iwmult",$1/$2);this.setAttribute("_ihmult",1)
}else{
this.setAttribute("_wmult",$1/$2);this.setAttribute("_hmult",1);this.setAttribute("_iwmult",1);this.setAttribute("_ihmult",$2/$1)
};this.setAttribute("_diwmult",_iwmult-_wmult);this.setAttribute("_dihmult",_ihmult-_hmult);this.updateX(true);this.updateY(true);this.setAttribute("loaded",true);intparent.interior.setAttribute("opacity",0);if(tls.waitforload!=true){
this.intparent.setAttribute("visible",true)
};intparent.interior.animate("opacity",1,200);this.txt.adjustDimensions()
}},"getImageURL",function($0){
var $1="";if($0!=""){
$1=this.datapath.xpathQuery("thumblocation/text()")
}else $1=this.datapath.xpathQuery("location/text()");return $1
},"setImage",function($0){
this.intparent.setAttribute("visible",false);this.intparent.interior.setSource($0)
},"startDragCheck",function(){
with(this){
this._mdtime=LzTimeKernel.getTimer();this.ddcdel.register(lz.Idle,"onidle");this._ddcxp=this.getMouse("x");this._ddcyp=this.getMouse("y")
}},"doDragCheck",function($0){
with(this){
var $1=this.getMouse("x")-this._ddcxp;var $2=this.getMouse("y")-this._ddcyp;if(LzTimeKernel.getTimer()-this._mdtime>mousedownBeforeDrag||5<Math.abs($1)+Math.abs($2)){
this.startDrag($1,$2)
}}},"startDrag",function($0,$1){
this.ddcdel.unregisterAll();this.setAttribute("draginitiator",true)
},"stopDrag",function(){
this.setAttribute("draginitiator",false)
},"setClipFactor",function($0){
this.clipfactor=$0;this.updateX(true);this.updateY(true)
},"updateX",function($0){
with(this){
if(!this.isinited)return;if(height!=width)this.setAttribute("height",width);if(_lastw==width&&$0!=true){
return
};this._lastw=width;var $1=_wmult+clipfactor*_diwmult;var $2=$1*(this.width-2*this.border);var $3=this.width/2-$2/2;var $4=$2+clipfactor*(width-$2-2*border);var $5=this.width/2-$4/2;intparent.setAttribute("x",$5);intparent.setAttribute("width",$4);borderbg.setAttribute("x",$5-this.border);borderbg.setAttribute("width",$4+2*this.border);intparent.interior.setAttribute("x",$3-$5);intparent.interior.setAttribute("width",$2);shadow.setAttribute("x",border+$5);shadow.setAttribute("width",$4+this.border)
}},"updateY",function($0){
with(this){
if(!this.isinited)return;if(width!=height)this.setAttribute("width",height);if(_lasth==height&&$0!=true){
return
};this._lasth=height;var $1=_hmult+clipfactor*_dihmult;var $2=$1*(this.height-2*this.border);var $3=this.height/2-$2/2;var $4=$2+clipfactor*(height-$2-2*border);var $5=this.height/2-$4/2;intparent.setAttribute("y",$5);intparent.setAttribute("height",$4);borderbg.setAttribute("y",$5-this.border);borderbg.setAttribute("height",$4+2*this.border);intparent.interior.setAttribute("height",$2);intparent.interior.setAttribute("y",$3-$5);shadow.setAttribute("y",border+$5);shadow.setAttribute("height",$4+this.border)
}},"shadow",void 0,"borderbg",void 0,"intparent",void 0,"txt",void 0],["tagname","photo","children",[{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0x0"),name:"shadow",opacity:0.5},"class":LzView},{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0xffffff"),name:"borderbg"},"class":LzView},{attrs:{$classrootdepth:1,clip:true,interior:void 0,name:"intparent",x:new LzOnceExpr("$m480"),y:new LzOnceExpr("$m481")},"class":$lzc$class_m490},{attrs:{$classrootdepth:1,$delegates:["ontext","$m488",null,"onvisible","$m489",null],fontsize:10,name:"txt",text:"",visible:false,width:100,y:new LzAlwaysExpr("$m486","$m487")},"class":$lzc$class_m491},{attrs:"interior","class":$lzc$class_userClassPlacement}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onwidth","updateX",null,"onheight","updateY",null,"onwidth","$m474",null,"onmousedown","$m475",null,"onmouseup","$m476",null,"ondata","$m477",null,"onload","$m479","$m478"],_dihmult:0,_diwmult:0,_hmult:0,_ihmult:0,_iwmult:0,_lasth:null,_lastw:null,_wmult:0,aspect:1,border:2,clickable:true,clipfactor:0,ddcdel:new LzOnceExpr("$m473"),doesdrag:false,draginitiator:false,height:75,intheight:new LzAlwaysExpr("$m471","$m472"),intwidth:new LzAlwaysExpr("$m469","$m470"),loaded:false,mousedownBeforeDrag:300,onplainclick:null,text:"description",title:"title",visible:new LzAlwaysExpr("$m467","$m468"),width:75},$lzc$class_photo.attributes)
}}})($lzc$class_photo);Class.make("$lzc$class_m492",$lzc$class_photo,["selected",void 0,"isselected",void 0,"rerunxpath",void 0,"$m402",function($0){
with(this){
if(parent.selman.isMultiSelect(null)){
parent.selman.select(this)
}else{
if(parent.lyt.isgrid){
parent.parent.details.setImage(this.getImageURL("t"),this.getImageURL(""));photoscontainer.lyt.setAttribute("dimmer",0.2);parent.transitionToDetails(this);parent.showPhotoDetails(true,this)
}else{
if(photoscontainer.detailphoto==this){
return
}else{
parent.parent.details.setImage(this.getImageURL("t"),this.getImageURL(""));parent.parent.details.title.setAttribute("text",this.title);parent.parent.details.info.setAttribute("text",this.text);parent.showPhotoDetails(true,this)
}};photoscontainer.detailphoto=this
}}},"setSelected",function($0){
this.setAttribute("isselected",$0);this.intparent.setAttribute("opacity",$0?0.5:1)
},"intersectsRectangle",function($0,$1,$2,$3){
with(this){
return x+width>$0&&x<$2&&y+height>$1&&y<$3
}},"$classrootdepth",void 0],["children",LzNode.mergeChildren([],$lzc$class_photo["children"]),"attributes",new LzInheritedHash($lzc$class_photo.attributes)]);Class.make("$lzc$class_m493",LzView,["_sx",void 0,"_sy",void 0,"$m403",function($0){
with(this){
this.setAttribute("updel",new LzDelegate(this,"update"))
}},"updel",void 0,"starter",function(){
with(this){
this.setAttribute("visible",true);this._sx=parent.getMouse("x");this._sy=parent.getMouse("y");this.updel.register(lz.Idle,"onidle");this.bringToFront();this.update()
}},"update",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};var $1=this.parent.getMouse("x");var $2=this.parent.getMouse("y");if($1<_sx){
this.setAttribute("x",$1);this.setAttribute("width",this._sx-$1-1)
}else{
this.setAttribute("x",this._sx);this.setAttribute("width",$1-this._sx+1)
};if($2<this._sy){
this.setAttribute("y",$2);this.setAttribute("height",this._sy-$2-1)
}else{
this.setAttribute("y",this._sy);this.setAttribute("height",$2-this._sy+1)
}}},"stopper",function($0,$1){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;

};this.setAttribute("visible",false);this.updel.unregisterAll();this.parent.selectInRectangle(this._sx,this._sy,this.parent.getMouse("x"),this.parent.getMouse("y"))
},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_spinner",LzView,["$m494",function($0){
this.play()
},"counter",void 0,"$m495",function(){
with(this){
var $0=lz.Idle;return $0
}},"$m496",function($0){
if(!this.visible)return;this.setAttribute("counter",(this.counter+1)%6);if(this.counter==0){
var $1=(this.frame+1)%6;this.setAttribute("frame",$1==0?6:$1)
}}],["tagname","spinner","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onclick","$m494",null,"onidle","$m496","$m495"],clickable:true,counter:1,resource:"$LZ6",visible:false},$lzc$class_spinner.attributes)
}}})($lzc$class_spinner);Class.make("$lzc$class_m440",LzView,["$m341",function($0){
with(this){
var $1=classroot.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m342",function(){
with(this){
return [classroot,"width"]
}},"$m343",function($0){
with(this){
var $1=classroot.height-50;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m344",function(){
with(this){
return [classroot,"height"]
}},"detailphoto",void 0,"initialDone",void 0,"$m345",function($0){
with(this){
this.setAttribute("doneDel",new LzDelegate(this,"initialReplicationDone"))
}},"doneDel",void 0,"isRectangleSelecting",void 0,"$m346",function($0){
with(this){
if(lyt.isgrid)rubberband.starter()
}},"$m347",function($0){
with(this){
rubberband.stopper()
}},"$m348",function(){
var $0=this.lyt.transitiontogrid_anm;return $0
},"$m349",function($0){
with(this){
this.lyt.isgrid=true;this.lyt.myreset();this.lyt.setAttribute("textvisible",true);this.lyt.update();this.showPhotoDetails(false);tls.enableZoom()
}},"$m350",function(){
with(this){
var $0=classroot;return $0
}},"$m351",function($0){
with(this){
gDebug(this,"ondata reference=classroot");this.initialReplicationDone();if(!classroot.datapath.p){
return
};tls.setpPagingParams(classroot.datapath.p.getElementsByTagName("list")[0].childNodes.length);this.watchforlast();photoscontainer.lyt.calcpageparams=true
}},"displaytext",function($0){
with(this){
var $1;for($1 in photoscontainer.lyt.subviews){
if(photoscontainer.lyt.subviews[$1].txt&&photoscontainer.lyt.subviews[$1].txt.setVisible)photoscontainer.lyt.subviews[$1].txt.setAttribute("visible",$0)
}}},"transitionToDetails",function($0){
with(this){
var $1=$0.intparent.interior;var $2=$1.width>$1.height?$1.width+2:$1.height+2;classroot.details.ph.setAttribute("width",$2);classroot.details.ph.setAttribute("height",$2);classroot.details.setAttribute("visible",true);this.lyt.setAttribute("textvisible",false);this.lyt.setAttribute("isgrid",false);this.lyt.updateOnIdle(true);this.lyt.transitiontolinear_anm.grp.yspacing.setAttribute("to",-photoscontainer.lyt.photodimension*photoscontainer.lyt.photoscale);this.lyt.transitiontolinear_anm.doStart()
}},"transitionToGrid",function(){
this.lyt.transitiontogrid_anm.doStart()
},"showPhotoDetails",function($0,$1){
with(this){
switch(arguments.length){
case 1:
$1=null;

};if($0){
classroot.details.setAttribute("visible",true);classroot.tools.disableZoom()
}else{
classroot.details.setAttribute("visible",false);classroot.details.info.setAttribute("x",classroot.width+10)
}}},"watchforlast",function(){
if(!this.ph["clones"])return;var $0=this.ph.clones[this.ph.clones.length-1];this.doneDel.unregisterAll();if($0!=null){
this.doneDel.register($0,"oninit")
}},"initialReplicationDone",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};photoscontainer.lyt.unlock();photoscontainer.lyt.update();photoscontainer.setAttribute("visible",true);spnr.setAttribute("visible",false)
}},"selectInRectangle",function($0,$1,$2,$3){
with(this){
if(!selman.isMultiSelect(null)){
selman.clearSelection()
};this.setAttribute("isRectangleSelecting",true);if($0>$2){
var $4=$0;$0=$2;$2=$4
};if($1>$3){
var $4=$1;$1=$3;$3=$4
};for(var $5=subviews.length-1;$5>=0;$5--){
var $6=subviews[$5];if(!($6 instanceof lz.photo))continue;if($6.intersectsRectangle($0,$1,$2,$3)){
selman.select($6)
}};this.setAttribute("isRectangleSelecting",false)
}},"lyt",void 0,"selman",void 0,"ph",void 0,"rubberband",void 0,"spnr",void 0,"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,$delegates:["onstop","$m384","$m383"],name:"lyt",pageNext:void 0,pagePrev:void 0,photodimension:70,photoscale:new LzAlwaysExpr("$m352","$m353"),pivot_x:50,pivot_y:0,pivotindex:0,skew:0,spacing:50,transitiontogrid_anm:void 0,transitiontolinear_anm:void 0,xinset:0,yinset:50},"class":$lzc$class_m447},{attrs:{$classrootdepth:2,name:"selman",toggle:false},"class":$lzc$class_m466},{attrs:{$classrootdepth:2,$delegates:["onplainclick","$m402",null],datapath:"list/img",isselected:false,name:"ph",rerunxpath:true,selected:false,visible:true},"class":$lzc$class_m492},{attrs:{$classrootdepth:2,_sx:0,_sy:0,bgcolor:LzColorUtils.convertColor("0xbb"),name:"rubberband",opacity:0.3,options:{ignorelayout:true},updel:new LzOnceExpr("$m403"),visible:false},"class":$lzc$class_m493},{attrs:{$classrootdepth:2,align:"center",name:"spnr",valign:"middle"},"class":$lzc$class_spinner}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m512",LzText,["$m497",function($0){
with(this){
var $1=classroot.parent.width-43;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m498",function(){
with(this){
return [classroot.parent,"width"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m513",LzText,["$m499",function($0){
with(this){
var $1=classroot.parent.width-48;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m500",function(){
with(this){
return [classroot.parent,"width"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m515",LzAnimator,["$m501",function($0){
with(this){
var $1=classroot.parent.width-6;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m502",function(){
with(this){
return [classroot.parent,"width"]
}},"$m503",function($0){
with(this){
this.setAttribute("duration",classroot.parent.anm_multipler*300)
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m516",LzAnimator,["$m504",function($0){
with(this){
var $1=classroot.parent.height-150;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m505",function(){
with(this){
return [classroot.parent,"height"]
}},"$m506",function($0){
with(this){
this.setAttribute("duration",classroot.parent.anm_multipler*300)
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m517",LzAnimator,["$m507",function($0){
with(this){
this.setAttribute("duration",classroot.parent.anm_multipler*300)
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m518",LzAnimator,["$m508",function($0){
with(this){
this.setAttribute("duration",classroot.parent.anm_multipler*300)
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m526",LzAnimator,["$m521",function($0){
with(this){
this.setAttribute("duration",parent.fadein)
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m527",LzView,["$m522",function($0){
var $1=this.parent.width-2;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}},"$m523",function(){
return [this.parent,"width"]
},"$m524",function($0){
var $1=this.parent.height-2;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}},"$m525",function(){
return [this.parent,"height"]
},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_naturalimgview",LzView,["fadein",void 0,"$m519",function(){
with(this){
var $0=interior;return $0
}},"$m520",function($0){
this.setDimensions();this.setAttribute("visible",true);this.anm_opacity.doStart()
},"setDimensions",function(){
with(this){
var $0=this.interior;if($0["resourceheight"]==null){
return
};var $1=$0.resourceheight;var $2=$0.resourcewidth;if($2==0){
return
};var $3=$1/$2;var $4=this.parent.height;var $5=this.parent.width;if($5==0){
return
};var $6=$4/$5;if($6<$3){
this.setAttribute("height",$4);this.setAttribute("width",Math.round($4/$3))
}else if($6>$3){
this.setAttribute("height",Math.round($5*$3));this.setAttribute("width",$5)
}else{
this.setAttribute("height",$4);this.setAttribute("width",$5)
}}},"anm_opacity",void 0,"interior",void 0],["tagname","naturalimgview","children",[{attrs:{$classrootdepth:1,attribute:"opacity",duration:new LzOnceExpr("$m521"),name:"anm_opacity",to:1},"class":$lzc$class_m526},{attrs:{$classrootdepth:1,height:new LzAlwaysExpr("$m524","$m525"),name:"interior",stretches:"both",width:new LzAlwaysExpr("$m522","$m523"),x:1,y:1},"class":$lzc$class_m527}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onload","$m520","$m519"],fadein:500,opacity:0},$lzc$class_naturalimgview.attributes)
}}})($lzc$class_naturalimgview);Class.make("$lzc$class_m528",$lzc$class_naturalimgview,["$m509",function(){
with(this){
var $0=anm_opacity;return $0
}},"$m510",function($0){
with(this){
var $1=classroot.photosource_m;if($1)this.parent.intparent2.interior.setSource($1)
}},"$classrootdepth",void 0],["children",LzNode.mergeChildren([],$lzc$class_naturalimgview["children"]),"attributes",new LzInheritedHash($lzc$class_naturalimgview.attributes)]);Class.make("$lzc$class_m514",LzView,["posme",void 0,"intparent",void 0,"intparent2",void 0,"reset",function(){
this.setAttribute("visible",true);this.intparent2.setAttribute("opacity",0);this.intparent.setAttribute("opacity",0)
},"$m511",function($0){
this.intparent.setDimensions();this.intparent2.setDimensions()
},"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,name:"posme",process:"simultaneous",start:false},children:[{attrs:{$classrootdepth:3,attribute:"width",duration:new LzOnceExpr("$m503"),to:new LzAlwaysExpr("$m501","$m502")},"class":$lzc$class_m515},{attrs:{$classrootdepth:3,attribute:"height",duration:new LzOnceExpr("$m506"),to:new LzAlwaysExpr("$m504","$m505")},"class":$lzc$class_m516},{attrs:{$classrootdepth:3,attribute:"x",duration:new LzOnceExpr("$m507"),to:3},"class":$lzc$class_m517},{attrs:{$classrootdepth:3,attribute:"y",duration:new LzOnceExpr("$m508"),to:35},"class":$lzc$class_m518}],"class":LzAnimatorGroup},{attrs:{$classrootdepth:2,$delegates:["onstart","$m510","$m509"],$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="gPhV";gPhV=$0
}else if(gPhV===$0){
gPhV=null;$0.id=null
}},align:"center",id:"gPhV",name:"intparent"},"class":$lzc$class_m528},{attrs:{$classrootdepth:2,align:"center",fadein:250,name:"intparent2"},"class":$lzc$class_naturalimgview}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_detailsview",LzView,["photosource_t",void 0,"photosource_m",void 0,"setImage",function($0,$1){
this.setAttribute("photosource_t",$0);this.setAttribute("photosource_m",$1);this.ph.reset();this.ph.intparent.interior.setSource($0)
},"title",void 0,"info",void 0,"ph",void 0],["tagname","detailsview","children",[{attrs:{$classrootdepth:1,fontsize:14,fontstyle:"bold",name:"title",text:"Titolo",width:new LzAlwaysExpr("$m497","$m498"),x:-300,y:1},"class":$lzc$class_m512},{attrs:{$classrootdepth:1,fontsize:12,name:"info",text:"Breve Descrizione",width:new LzAlwaysExpr("$m499","$m500"),x:-300,y:16},"class":$lzc$class_m513},{attrs:{$classrootdepth:1,$delegates:["onheight","$m511",null],intparent:void 0,intparent2:void 0,name:"ph",posme:void 0},"class":$lzc$class_m514}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({photosource_m:"",photosource_t:"",visible:false},$lzc$class_detailsview.attributes)
}}})($lzc$class_detailsview);Class.make("$lzc$class_m530",LzView,["$m418",function($0){
with(this){
var $1=90*photoscontainer.width*photoscontainer.height/(1665*829);if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m419",function(){
with(this){
return [photoscontainer,"width",photoscontainer,"height"]
}},"$m420",function($0){
this.startdragging()
},"$m421",function($0){
this.stopdragging()
},"startdragging",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};dragging.setAttribute("applied",true);photoscontainer.lyt.calcpageparams=true;photoscontainer.lyt.setAttribute("textvisible",false);photoscontainer.lyt.update()
}},"stopdragging",function(){
with(this){
dragging.setAttribute("applied",false);photoscontainer.lyt.setAttribute("textvisible",true);photoscontainer.lyt.update()
}},"dragging",void 0,"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:3,drag_axis:"x",drag_max_x:117,drag_min_x:5,name:"dragging"},"class":$lzc$class_dragstate}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m531",LzView,["$m422",function($0){},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m532",LzView,["$m423",function($0){
with(this){
var $1=canvas.bgcolor;if($1!==this["bgcolor"]||!this.inited){
this.setAttribute("bgcolor",$1)
}}},"$m424",function(){
with(this){
return [canvas,"bgcolor"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m533",LzView,["$m425",function($0){
with(this){
photoscontainer.transitionToGrid()
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m534",LzText,["$m426",function($0){
var $1=173-this.width;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}},"$m427",function(){
return [this,"width"]
},"$m428",function($0){
with(this){
var $1=!parent.waitforload;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m429",function(){
with(this){
return [parent,"waitforload"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m535",LzText,["$m430",function($0){
with(this){
var $1=!parent.waitforload;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m431",function(){
with(this){
return [parent,"waitforload"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m536",LzText,["$m432",function($0){
with(this){
var $1=!parent.waitforload;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m433",function(){
with(this){
return [parent,"waitforload"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m537",LzText,["$m434",function($0){
var $1=170-this.width/2;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}},"$m435",function(){
return [this,"width"]
},"$m436",function($0){
with(this){
var $1=parent.waitforload;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m437",function(){
with(this){
return [parent,"waitforload"]
}},"$classrootdepth",void 0],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m538",$lzc$class_button,["$m438",function($0){
with(this){
parent.displayPrevPage()
}},"$classrootdepth",void 0],["children",LzNode.mergeChildren([{attrs:{$classrootdepth:3,opacity:0.7,resource:"$LZ4",x:3,y:3},"class":LzView}],$lzc$class_button["children"]),"attributes",new LzInheritedHash($lzc$class_button.attributes)]);Class.make("$lzc$class_m539",$lzc$class_button,["$m439",function($0){
with(this){
parent.displayNextPage()
}},"$classrootdepth",void 0],["children",LzNode.mergeChildren([{attrs:{$classrootdepth:3,opacity:0.7,resource:"$LZ5",x:3,y:3},"class":LzView}],$lzc$class_button["children"]),"attributes",new LzInheritedHash($lzc$class_button.attributes)]);Class.make("$lzc$class_m529",LzView,["$m404",function($0){
with(this){
var $1=classroot.height-50;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m405",function(){
with(this){
return [classroot,"height"]
}},"$m406",function($0){
with(this){
var $1=classroot.width-216;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m407",function(){
with(this){
return [classroot,"width"]
}},"$m408",function($0){
with(this){
var $1=Math.max(1,Math.min(1+this.thmb.x/60,3));if($1!==this["zoomscale"]||!this.inited){
this.setAttribute("zoomscale",$1)
}}},"$m409",function(){
with(this){
return [this.thmb,"x"].concat(Math["$lzc$max_dependencies"]?Math["$lzc$max_dependencies"](this,Math,1,Math.min(1+this.thmb.x/60,3)):[]).concat(Math["$lzc$min_dependencies"]?Math["$lzc$min_dependencies"](this,Math,1+this.thmb.x/60,3):[])
}},"zoomscale",void 0,"numberofpages",void 0,"pagesize",void 0,"numberofphotos",void 0,"startindex",void 0,"nextstartindex",void 0,"endindex",void 0,"nextendindex",void 0,"waitforload",void 0,"$m410",function(){
with(this){
var $0=classroot;return $0
}},"$m411",function($0){
with(this){
this.setAttribute("waitforload",false);photoscontainer.lyt.calcparams=true
}},"$m412",function(){
with(this){
var $0=photoscontainer.lyt;return $0
}},"$m413",function($0){
with(this){
this.endindex=this.startindex+photoscontainer.lyt.pagesize-1;this.displayRange(this.startindex,this.endindex)
}},"$m414",function(){
with(this){
var $0=photoscontainer.lyt.pageNext;return $0
}},"$m415",function($0){
with(this){
this.displayPage();photoscontainer.lyt.calcpageparams=true;if(photoscontainer.lyt.isgrid){
photoscontainer.lyt.setAttribute("pivot_x",50)
}else{
photoscontainer.lyt.setAttribute("pivot_x",10);photoscontainer.lyt.setAttribute("pivotindex",0)
}}},"$m416",function(){
with(this){
var $0=photoscontainer.lyt.pagePrev;return $0
}},"$m417",function($0){
with(this){
this.displayPage();photoscontainer.lyt.calcpageparams=true;if(photoscontainer.lyt.isgrid){
photoscontainer.lyt.setAttribute("pivot_x",50)
}else{
photoscontainer.lyt.setAttribute("pivot_x",10);photoscontainer.lyt.setAttribute("pivotindex",0)
}}},"reset",function(){
with(this){
this.startindex=1;this.endindex=this.startindex+photoscontainer.lyt.pagesize-1;this.displayRange(this.startindex,this.endindex)
}},"resetOnLoad",function(){
with(this){
this.loadtext.setAttribute("text","Loading...");this.setAttribute("waitforload",true);this.setAttribute("startindex",1);this.setAttribute("nextstartindex",1);this.setAttribute("endindex",1+photoscontainer.lyt.totalitems);this.setAttribute("nextendindex",1+photoscontainer.lyt.totalitems)
}},"enableZoom",function(){
with(this){
thmb.setAttribute("visible",true);zoomScreen.setAttribute("visible",false);hidedetails.setAttribute("visible",false)
}},"disableZoom",function(){
with(this){
thmb.setAttribute("visible",false);zoomScreen.setAttribute("visible",true);hidedetails.setAttribute("visible",true)
}},"displayNextPage",function(){
with(this){
this.nextstartindex=this.endindex+1;this.nextendindex=Math.min(this.nextstartindex+photoscontainer.lyt.pagesize-1,this.numberofphotos);if(this.nextstartindex<=this.numberofphotos){
if(this.nextendindex==this.numberofphotos){
this.nextstartindex=this.nextendindex-photoscontainer.lyt.totalitems+1
};photoscontainer.lyt.calcpageparams=false;photoscontainer.lyt.pageNext.doStart()
}}},"displayPrevPage",function(){
with(this){
if(this.startindex>1){
if(photoscontainer.lyt.totalitems<=photoscontainer.lyt.pagesize){
this.nextendindex=this.startindex-1;this.nextstartindex=Math.max(this.nextendindex-photoscontainer.lyt.totalitems+1,1);if(this.nextstartindex==1)this.nextendindex=photoscontainer.lyt.totalitems
}else{
this.nextendindex=this.startindex-1;this.nextstartindex=Math.max(this.nextendindex-photoscontainer.lyt.pagesize+1,1)
};photoscontainer.lyt.calcpageparams=false;photoscontainer.lyt.pagePrev.doStart()
}else if(photoscontainer.lyt.currentpage>1){
this.loadtext.setAttribute("text","Prev 100...");this.setAttribute("waitforload",true);this.nextendindex=this.numberofphotos;this.nextstartindex=this.nextendindex-photoscontainer.lyt.totalitems+1;photoscontainer.lyt.setAttribute("currentpage",Number(photoscontainer.lyt.currentpage)-1)
}}},"displayPage",function(){
with(this){
var $0=25;var $1=this.nextstartindex+$0;var $2="list/img["+this.nextstartindex+"-"+$1+"]";photoscontainer.ph.setAttribute("datapath",$2);photoscontainer.watchforlast();this.startindex=this.nextstartindex;this.endindex=this.nextendindex
}},"displayRange",function($0,$1){
with(this){
this.firstphotoindex.setAttribute("text",$0+photoscontainer.lyt.perpage*(photoscontainer.lyt.currentpage-1));this.lastphotoindex.setAttribute("text",$1+photoscontainer.lyt.perpage*(photoscontainer.lyt.currentpage-1))
}},"setpPagingParams",function($0){
with(this){
this.setAttribute("numberofphotos",Number($0))
}},"zoombg",void 0,"thmb",void 0,"zoomScreen",void 0,"zoomColor",void 0,"hidedetails",void 0,"firstphotoindex",void 0,"lastphotoindex",void 0,"loadtext",void 0,"pageprev",void 0,"pagenext",void 0,"$classrootdepth",void 0],["children",[{attrs:{$classrootdepth:2,name:"zoombg",resource:"$LZ1",x:0,y:23},"class":LzView},{attrs:{$classrootdepth:2,$delegates:["onmousedown","$m420",null,"onmouseup","$m421",null],clickable:true,dragging:void 0,name:"thmb",resource:"$LZ2",x:new LzAlwaysExpr("$m418","$m419"),y:24},"class":$lzc$class_m530},{attrs:{$classrootdepth:2,$delegates:["onclick","$m422",null],bgcolor:LzColorUtils.convertColor("0x7f7f7f"),clickable:false,height:30,name:"zoomScreen",opacity:0.5,showhandcursor:false,visible:false,width:130,x:0,y:23},"class":$lzc$class_m531},{attrs:{$classrootdepth:2,bgcolor:new LzAlwaysExpr("$m423","$m424"),height:30,name:"zoomColor",opacity:0.35,width:130,x:0,y:23},"class":$lzc$class_m532},{attrs:{$classrootdepth:2,$delegates:["onclick","$m425",null],clickable:true,name:"hidedetails",resource:"$LZ3",visible:false,x:53,y:28},"class":$lzc$class_m533},{attrs:{$classrootdepth:2,fgcolor:LzColorUtils.convertColor("0x463e9d"),fontsize:9,name:"firstphotoindex",visible:new LzAlwaysExpr("$m428","$m429"),x:new LzAlwaysExpr("$m426","$m427"),y:7},"class":$lzc$class_m534},{attrs:{$classrootdepth:2,fgcolor:LzColorUtils.convertColor("0x463e9d"),text:"-",visible:new LzAlwaysExpr("$m430","$m431"),x:175,y:4},"class":$lzc$class_m535},{attrs:{$classrootdepth:2,fgcolor:LzColorUtils.convertColor("0x463e9d"),fontsize:9,name:"lastphotoindex",visible:new LzAlwaysExpr("$m432","$m433"),x:185,y:7},"class":$lzc$class_m536},{attrs:{$classrootdepth:2,fgcolor:LzColorUtils.convertColor("0x463e9d"),fontsize:9,name:"loadtext",visible:new LzAlwaysExpr("$m436","$m437"),x:new LzAlwaysExpr("$m434","$m435"),y:7},"class":$lzc$class_m537},{attrs:{$classrootdepth:2,$delegates:["onclick","$m438",null],clickable:true,height:35,name:"pageprev",width:35,x:145,y:21},"class":$lzc$class_m538},{attrs:{$classrootdepth:2,$delegates:["onclick","$m439",null],clickable:true,height:35,name:"pagenext",width:35,x:178,y:21},"class":$lzc$class_m539}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_OMWImageGallery",LzView,["allowDownload",void 0,"zoomscale",void 0,"anm_multipler",void 0,"appStyle",void 0,"photos",void 0,"details",void 0,"tools",void 0],["tagname","OMWImageGallery","children",[{attrs:{$classrootdepth:1,name:"appStyle"},"class":$lzc$class_style},{attrs:{$classrootdepth:1,$delegates:["onmousedown","$m346",null,"onmouseup","$m347",null,"onstop","$m349","$m348","ondata","$m351","$m350"],$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="photoscontainer";photoscontainer=$0
}else if(photoscontainer===$0){
photoscontainer=null;$0.id=null
}},align:"center",clickable:true,detailphoto:null,doneDel:new LzOnceExpr("$m345"),height:new LzAlwaysExpr("$m343","$m344"),id:"photoscontainer",initialDone:false,isRectangleSelecting:false,lyt:void 0,name:"photos",ph:void 0,rubberband:void 0,selman:void 0,showhandcursor:false,spnr:void 0,visible:false,width:new LzAlwaysExpr("$m341","$m342"),y:3},"class":$lzc$class_m440},{attrs:{$classrootdepth:1,name:"details",visible:false},"class":$lzc$class_detailsview},{attrs:{$classrootdepth:1,$delegates:["ondata","$m411","$m410","onpageend","$m413","$m412","onstop","$m415","$m414","onstop","$m417","$m416"],$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="tls";tls=$0
}else if(tls===$0){
tls=null;$0.id=null
}},endindex:18,firstphotoindex:void 0,hidedetails:void 0,id:"tls",lastphotoindex:void 0,loadtext:void 0,name:"tools",nextendindex:18,nextstartindex:1,numberofpages:0,numberofphotos:0,pagenext:void 0,pageprev:void 0,pagesize:18,startindex:1,thmb:void 0,waitforload:false,x:new LzAlwaysExpr("$m406","$m407"),y:new LzAlwaysExpr("$m404","$m405"),zoomColor:void 0,zoomScreen:void 0,zoombg:void 0,zoomscale:new LzAlwaysExpr("$m408","$m409")},"class":$lzc$class_m529}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({allowDownload:true,anm_multipler:1,height:800,width:900,zoomscale:1},$lzc$class_OMWImageGallery.attributes)
}}})($lzc$class_OMWImageGallery);canvas.LzInstantiateView({"class":lz.script,attrs:{script:function(){
gDebug=void 0;gDebug=function($0,$1,$2,$3){
switch(arguments.length){
case 2:
$2="";
case 3:
$3="";

};var $4="";if($2!="")$4=" with: ".concat($2);if($3!="")$4=$4.concat(" say: ").concat($3);Debug.write("called ",$0,"::",$1,$4)
}}}},1);Class.make("$lzc$class_fuzzyStyle",$lzc$class_style,["$m540",function($0){
with(this){
this.setAttribute("basecolor",LzColorUtils.convertColor("0x330033"))
}},"$m541",function($0){
with(this){
this.setAttribute("canvascolor",LzColorUtils.convertColor("0x996699"))
}},"$m542",function($0){
with(this){
this.setAttribute("bgcolor",LzColorUtils.convertColor("0xff66cc"))
}},"$m543",function($0){
with(this){
this.setAttribute("textcolor",LzColorUtils.convertColor("0x00cc00"))
}}],["tagname","fuzzyStyle","attributes",new LzInheritedHash($lzc$class_style.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({basecolor:new LzOnceExpr("$m540"),bgcolor:new LzOnceExpr("$m542"),canvascolor:new LzOnceExpr("$m541"),textcolor:new LzOnceExpr("$m543")},$lzc$class_fuzzyStyle.attributes)
}}})($lzc$class_fuzzyStyle);Class.make("$lzc$class_yellowStyle",$lzc$class_style,["$m544",function($0){
with(this){
this.setAttribute("basecolor",LzColorUtils.convertColor("0xffff00"))
}},"$m545",function($0){
with(this){
this.setAttribute("canvascolor",LzColorUtils.convertColor("0xccff66"))
}},"$m546",function($0){
with(this){
this.setAttribute("bgcolor",LzColorUtils.convertColor("0x888822"))
}},"$m547",function($0){
with(this){
this.setAttribute("textcolor",LzColorUtils.convertColor("0x0000ff"))
}}],["tagname","yellowStyle","attributes",new LzInheritedHash($lzc$class_style.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({basecolor:new LzOnceExpr("$m544"),bgcolor:new LzOnceExpr("$m546"),canvascolor:new LzOnceExpr("$m545"),textcolor:new LzOnceExpr("$m547")},$lzc$class_yellowStyle.attributes)
}}})($lzc$class_yellowStyle);Class.make("$lzc$class_redStyle",$lzc$class_style,["$m548",function($0){
with(this){
this.setAttribute("basecolor",LzColorUtils.convertColor("0xff0000"))
}},"$m549",function($0){
with(this){
this.setAttribute("canvascolor",LzColorUtils.convertColor("0xff3333"))
}},"$m550",function($0){
with(this){
this.setAttribute("bgcolor",LzColorUtils.convertColor("0x992222"))
}},"$m551",function($0){
with(this){
this.setAttribute("textcolor",LzColorUtils.convertColor("0x000000"))
}}],["tagname","redStyle","attributes",new LzInheritedHash($lzc$class_style.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({basecolor:new LzOnceExpr("$m548"),bgcolor:new LzOnceExpr("$m550"),canvascolor:new LzOnceExpr("$m549"),textcolor:new LzOnceExpr("$m551")},$lzc$class_redStyle.attributes)
}}})($lzc$class_redStyle);Class.make("$lzc$class_whiteStyle",$lzc$class_whitestyle,null,["tagname","whiteStyle","attributes",new LzInheritedHash($lzc$class_whitestyle.attributes)]);Class.make("$lzc$class_silverStyle",$lzc$class_silverstyle,null,["tagname","silverStyle","attributes",new LzInheritedHash($lzc$class_silverstyle.attributes)]);Class.make("$lzc$class_blueStyle",$lzc$class_bluestyle,null,["tagname","blueStyle","attributes",new LzInheritedHash($lzc$class_bluestyle.attributes)]);Class.make("$lzc$class_greenStyle",$lzc$class_greenstyle,null,["tagname","greenStyle","attributes",new LzInheritedHash($lzc$class_greenstyle.attributes)]);Class.make("$lzc$class_goldStyle",$lzc$class_goldstyle,null,["tagname","goldStyle","attributes",new LzInheritedHash($lzc$class_goldstyle.attributes)]);Class.make("$lzc$class_purpleStyle",$lzc$class_purplestyle,null,["tagname","purpleStyle","attributes",new LzInheritedHash($lzc$class_purplestyle.attributes)]);appData=canvas.lzAddLocalData("appData","<data />",false,false);appData==true;Class.make("$lzc$class_m561",$lzc$class_OMWImageGallery,["$m556",function($0){
with(this){
var $1=parent.height-44;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m557",function(){
with(this){
return [parent,"height"]
}},"$m558",function($0){
with(this){
var $1=parent.width-19;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m559",function(){
with(this){
return [parent,"width"]
}}],["children",LzNode.mergeChildren([],$lzc$class_OMWImageGallery["children"]),"attributes",new LzInheritedHash($lzc$class_OMWImageGallery.attributes)]);Class.make("$lzc$class_m560",$lzc$class_window,["$m552",function($0){
with(this){
var $1=canvas.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m553",function(){
with(this){
return [canvas,"height"]
}},"$m554",function($0){
with(this){
var $1=canvas.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m555",function(){
with(this){
return [canvas,"width"]
}},"openGallery",void 0],["children",LzNode.mergeChildren([{attrs:{$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="OMWApp";OMWApp=$0
}else if(OMWApp===$0){
OMWApp=null;$0.id=null
}},datapath:"appData:/gallery/",height:new LzAlwaysExpr("$m556","$m557"),id:"OMWApp",name:"openGallery",width:new LzAlwaysExpr("$m558","$m559")},"class":$lzc$class_m561}],$lzc$class_window["children"]),"attributes",new LzInheritedHash($lzc$class_window.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
appContainer=$0
}else if(appContainer===$0){
appContainer=null
}},closeable:true,height:new LzAlwaysExpr("$m552","$m553"),name:"appContainer",openGallery:void 0,resizable:false,title:"Open Multimedia Web Suite :: Video Player",width:new LzAlwaysExpr("$m554","$m555")},"class":$lzc$class_m560},104);lz["basefocusview"]=$lzc$class_basefocusview;lz["focusoverlay"]=$lzc$class_focusoverlay;lz["_componentmanager"]=$lzc$class__componentmanager;lz["style"]=$lzc$class_style;lz["whitestyle"]=$lzc$class_whitestyle;lz["silverstyle"]=$lzc$class_silverstyle;lz["bluestyle"]=$lzc$class_bluestyle;lz["greenstyle"]=$lzc$class_greenstyle;lz["goldstyle"]=$lzc$class_goldstyle;lz["purplestyle"]=$lzc$class_purplestyle;lz["statictext"]=$lzc$class_statictext;lz["basecomponent"]=$lzc$class_basecomponent;lz["basebutton"]=$lzc$class_basebutton;lz["swatchview"]=$lzc$class_swatchview;lz["button"]=$lzc$class_button;lz["dragstate"]=$lzc$class_dragstate;lz["resizestate"]=$lzc$class_resizestate;lz["resizestatemin"]=$lzc$class_resizestatemin;lz["basewindow"]=$lzc$class_basewindow;lz["stableborderlayout"]=$lzc$class_stableborderlayout;lz["resizeview_x"]=$lzc$class_resizeview_x;lz["resizeview_y"]=$lzc$class_resizeview_y;lz["windowpanel"]=$lzc$class_windowpanel;lz["simplelayout"]=$lzc$class_simplelayout;lz["resizelayout"]=$lzc$class_resizelayout;lz["modaldialog"]=$lzc$class_modaldialog;lz["alert"]=$lzc$class_alert;lz["window"]=$lzc$class_window;lz["pivotlayout"]=$lzc$class_pivotlayout;lz["photo"]=$lzc$class_photo;lz["spinner"]=$lzc$class_spinner;lz["naturalimgview"]=$lzc$class_naturalimgview;lz["detailsview"]=$lzc$class_detailsview;lz["OMWImageGallery"]=$lzc$class_OMWImageGallery;lz["fuzzyStyle"]=$lzc$class_fuzzyStyle;lz["yellowStyle"]=$lzc$class_yellowStyle;lz["redStyle"]=$lzc$class_redStyle;lz["whiteStyle"]=$lzc$class_whiteStyle;lz["silverStyle"]=$lzc$class_silverStyle;lz["blueStyle"]=$lzc$class_blueStyle;lz["greenStyle"]=$lzc$class_greenStyle;lz["goldStyle"]=$lzc$class_goldStyle;lz["purpleStyle"]=$lzc$class_purpleStyle;canvas.initDone();