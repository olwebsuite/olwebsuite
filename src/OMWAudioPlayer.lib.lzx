<?xml version="1.0" encoding="UTF-8"?>
<!--

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Open Multimedia Web Suite.

Open Multimedia Web Suite is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Open Multimedia Web Suite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Open Multimedia Web Suite.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.
For all other info read README.

-->

<library>
	<class name="OMWAudioPlayer" extends="view">
		<attribute name="height"		value="200" />
		<attribute name="width"			value="420" />
		<attribute name="startPlaying"	value="false"							type="boolean" />
		<attribute name="allowDownload" value="true"							type="boolean" />
		<attribute name="sponsorLink" 	value="http://launchpad.net/olwebsuite"	type="string"  />
		<style name="appStyle"/>
		
		<OMWAudioOutput name="audioOutput" volume="${classroot.vslider.volume}" >
			<attribute name="pointer"		value="1"						type="number"/>
			<datapath xpath="playlist/trackList/" />
			<handler name="ondata">
				//gDebug(this, "ondata");
				setAttribute("pointer", 1);
				classroot.vslider.completeInstantiation();
			</handler>
			<handler name="onpointer">
				<![CDATA[
				//gDebug(this, "onpointer", String(pointer));
				
				if (pointer < 1) 
				{
					setAttribute('pointer', datapath.xpathQuery("track/last()"));
				}
				else if (pointer > datapath.xpathQuery("track/last()"))
				{
					setAttribute('pointer', 1);
				}
				
				stop();
				
				
				
				var a:String = "track[" + String(pointer) + "]/";
				setAttribute('url', datapath.xpathQuery(a+ "location/text()"));
				
				// Set current item in playlist.
				if(classroot.playlistC.playlist.pointer != pointer)
				{
					classroot.playlistC.playlist.setAttribute("pointer", pointer);
				}
				
				if( pointer == 1 && !classroot.startPlaying)
				{
					pause();
					return;
				}
				
				setAttribute("paused", false);
				play();

				]]>
			</handler>
			<handler name="onlastframe">
				//gDebug(this, "onlastframe");
				setAttribute('pointer', parent.pointer + 1);
			</handler>
			<handler name="ontimeout">
				//gDebug(this, "ontimeout");
				setAttribute('pointer', parent.pointer + 1);
			</handler>
			<handler name="onerror">
				//gDebug(this, "onerror");
				setAttribute('pointer', parent.pointer + 1);
			</handler>
		</OMWAudioOutput>
		
		<view				name="trackImage"  width="114"	height="135" resource="http:image/nocover.png" stretches="both"  />
		<OMWMediaInfoShower name="infoShower" oWidth="114" oHeight="${classroot.height -20}" bgcolor="white" visible="false" />
		
		<view name="sponsor" resource="image/sponsor.png" width="15" height="${classroot.height -20}" x="115" options="ignorelayout" stretches="height">
			<handler name="onclick">lz.Browser.loadURL(classroot.sponsorLink)</handler>
		</view>

		<view name="playlistC" height="${classroot.height - 20}" width="${classroot.width - 131}" x="131" clip="true" options="ignorelayout">
			<OMWMediaStreamPlaylist name="playlist" width="${parent.width - parent.plScroller.width}" datapath="playlist/" player="$once{classroot.audioOutput}" bgcolor="${classroot.appStyle.bgcolor}" infoShower="$once{classroot.infoShower}"/>
			<vscrollbar name="plScroller" y="0" align="right"/>
		</view>
		
		<view name="controls" valign="bottom" width="60" height="20">
			<basebutton name="prevBtn"		x="0"  resource="OMWAudioPlayerPrevButton"		onclick="classroot.audioOutput.setAttribute('pointer', classroot.audioOutput.pointer-1)"/>
			<basebutton name="nextBtn"		x="20" resource="OMWAudioPlayerNextButton"		onclick="classroot.audioOutput.setAttribute('pointer', classroot.audioOutput.pointer+1)" />
			<basebutton name="downloadBtn"	x="40" resource="OMWAudioPlayerDownloadButton"	onclick="classroot.audioOutput.downloadTrack()" />
		</view>
		<OMWMediaStreamControl name="vslider" initstage="defer" x="60" valign="bottom" width="${parent.width -60}" height="20" ms="${classroot.audioOutput}" showTimeDisplay="true" showVolumeControl="true" color="0x00ff00" />
	</class>
	
	
	<resource name="OMWAudioPlayerPlShowButtonL">
		<frame src="image/arrow-left-double-up.png"/>
		<frame src="image/arrow-left-double-hover.png"/>
		<frame src="image/arrow-left-double-down.png"/>
	</resource>
	<resource name="OMWAudioPlayerPlShowButtonR">
		<frame src="image/arrow-right-double-up.png"/>
		<frame src="image/arrow-right-double-hover.png"/>
		<frame src="image/arrow-right-double-down.png"/>
	</resource>
	<resource name="OMWAudioPlayerPrevButton">
		<frame src="image/media-backward-up.png"/>
		<frame src="image/media-backward-hover.png"/>
		<frame src="image/media-backward-down.png"/>		
	</resource>
	<resource name="OMWAudioPlayerNextButton">
		<frame src="image/media-forward-up.png"/>
		<frame src="image/media-forward-hover.png"/>
		<frame src="image/media-forward-down.png"/>
	</resource>
	<resource name="OMWAudioPlayerDownloadButton">
		<frame src="image/download-up.png"/>
		<frame src="image/download-hover.png"/>
		<frame src="image/download-down.png"/>
	</resource>
	
	<include href="macros.lzx"/>
	<include href="OMWMediaStreamControl.lib.lzx"/>
	<include href="OMWMediaStreamPlaylist.lib.lzx"/>
	<include href="OMWMediaInfoShower.lib.lzx" />
	<include href="OMWAudioOutput.lib.lzx" />
	
</library>
