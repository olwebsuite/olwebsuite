function OMWEmbed(embedParams)
{
  this.params			= "?";
  this.appWidth			= "";
  this.appHeight		= "";
  this.OMWLocation		= "";
  this.OMWapp			= "";
  this.runtime			= "";
  this.runtimeVersion		= "";
  this.appId			= "";
  this.appenddivid		= null;

  this.Embed			= function()
  {  
    if(this.runtime == "dhtml")
    {
      var astr = "lz.embed.lfc(\"" + this.OMWLocation + "lps/includes/lfc/LFCdhtml.js\", \"" + this.OMWLocation + "lps/resources/\")";
      
      if(this.addJs(astr, "__laszlo__DHTML__RESOURCE_and_include__loader"))
      {
	this.execWhenVarLoaded("LzResourceLibrary", "this.EmbedDHTML()", 1000);
      }
      else
      {
	this.EmbedDHTML();
      }

      //alert(astr);
    }
    else if(this.runtime == "swf")
    {
      var appUrl = this.OMWLocation + this.OMWapp + ".lzx.swf" + this.runtimeVersion + ".swf"  + this.params;

      lz.embed.swf({url: appUrl, bgcolor: '#ffffff', width: this.appWidth, height: this.appHeight, id: this.appId, appenddivid: this.appenddivid, accessible: 'false'});
      //alert(appUrl);
    }
    else
    {
      alert("Wrong runtime selected! Recommended swf or dhtml");
    }
  }
  //#####################################################################################
  this.EmbedDHTML		= function()
  {
    var appUrl = this.OMWLocation + this.OMWapp + ".lzx.js" + this.params;

    lz.embed.dhtml({url: appUrl, approot: this.OMWLocation, bgcolor: '#ffffff', width: this.appWidth, height: this.appHeight, id: this.appId, appenddivid: this.appenddivid, accessible: 'false'});
    //alert(appUrl);
  }
  //#####################################################################################
  this.includeJsFile		= function(script_path, script_id)
  {
      var html_head = document.getElementsByTagName('head').item(0);
      if(document.getElementById(script_id))
      {
	//alert(script_id.concat(" already loaded"));
	return false;
      }
      var js = document.createElement('script');
      js.setAttribute('id', script_id);
      js.setAttribute('language', 'javascript');
      js.setAttribute('type', 'text/javascript');
      js.setAttribute('src', script_path);
      html_head.appendChild(js);
      return true;
  }
  //#####################################################################################
  this.addJs			= function(js, script_id)
  {
      var html_head = document.getElementsByTagName('head').item(0);
      if(document.getElementById(script_id))
      {
	//alert(script_id.concat(" already loaded"));
	return false;
      }
      var jS = document.createElement('script');
      jS.setAttribute('id', script_id);
      jS.setAttribute('language', 'javascript');
      jS.setAttribute('type', 'text/javascript');
      jS.appendChild(document.createTextNode(js));
      html_head.appendChild(jS);
      return true;
  }
  //#####################################################################################
  this.execWhenVarLoaded	= function(varName, jsCode, stepTime)
  {
    if(eval("typeof("+varName+")") == "undefined")
    {
      var thisC = this;
      setTimeout(function(){thisC.execWhenVarLoaded(varName, jsCode,stepTime)}, stepTime);
      //alert("var not found ".concat(runtime.concat(runtimeVersion)));
    }
    else
    {
      eval(jsCode);
      //alert("var found ".concat(varName));
    }
  }

  /*####################################
  #                                    #
  #                                    #
  #  END OF var and method definition  #
  #                                    #
  #                                    #
  #####################################*/

  for (var i=0; i<embedParams.length; i++) 
  {
    var argobj = embedParams[i];

         if(argobj.argname == "OMWLocation")	this.OMWLocation	= argobj.argvalue;
    else if(argobj.argname == "OMWapp")		this.OMWapp		= argobj.argvalue;
    else if(argobj.argname == "runtime")	this.runtime		= argobj.argvalue;
    else if(argobj.argname == "runtimeVersion")	this.runtimeVersion	= argobj.argvalue;
    else if(argobj.argname == "appId")		this.appId		= argobj.argvalue;
    else if(argobj.argname == "appenddivid")	this.appenddivid	= argobj.argvalue;
    else
    {
	   if(argobj.argname == "appWidth")	this.appWidth		= argobj.argvalue;
      else if(argobj.argname == "appHeight")	this.appHeight		= argobj.argvalue;

      this.params += argobj.argname;
      this.params += "=";
      this.params += argobj.argvalue;
      if( i < (embedParams.length - 1)) this.params += "&";
    }
  }

  if(this.includeJsFile(this.OMWLocation + "lps/includes/embed-compressed.js", "__laszlo__embed__compressed__"))
  {
    this.execWhenVarLoaded("lz", "this.Embed()", 1000);
  }
  else
  {
    this.Embed()
  }
}
